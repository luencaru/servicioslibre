# from __future__ import print_function
# from fabric.api import *
# from fabric.colors import green, red
#
# env.colorize_errors = True
#
#
# def server():
#     env.user = 'ubuntu'
#     env.host_string = '54.94.151.186'
#     env.key_filename = 'servicioslibre.pem'
#
#
# def deploy():
#     server()
#     home_path = "/home/ubuntu"
#     settings = "--settings='servicioslibre.settings.production'"
#     activate_env = "source {}/venv/bin/activate".format(home_path)
#     manage = "python manage.py"
#     print(green("Beginning Deploy:"))
#     #local("{} collectstatic --noinput {}".format(manage, settings))
#     with cd("{}/servicioslibre".format(home_path)):
#         run("git pull origin master")
#         run("{} && pip install -r requirements.txt".format(activate_env))
#         #run("{} && {} collectstatic --noinput {}".format(activate_env, manage, settings))
#         run("{} && {} syncdb {}".format(activate_env, manage, settings))
#         run("{} && {} migrate {}".format(activate_env, manage, settings))
#         sudo("/etc/init.d/nginx restart", pty=False)
#         sudo("supervisorctl restart gunicorn", pty=False)
#     print(green("Deploy Succesful :)"))
