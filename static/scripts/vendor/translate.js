angular.module('translate', ['pascalprecht.translate'])
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix:window.location.origin+ '/static/js/lang-',
            suffix: '.json'
        });
        // verwende deutsche Sprache
        $translateProvider.fallbackLanguage('en')
        //$translateProvider.preferredLanguage('en');
        $translateProvider.useMissingTranslationHandler('myCustomHandlerFactory');
    }])
    .run(['$rootScope','$window','$translate', function ($rootScope,$window,$translate) {
        var language=$window.navigator.userLanguage || $window.navigator.language
        language=language.split('-')[0];
        console.log(language,"LENGUAJE")
        if(language=="es"){
            $translate.uses(language).then(function(data){
                console.log("fadsfasd")
            },function(err){
                $translate.uses('en')
            })
        }else{
            $translate.uses('en')
        }
    }]).factory('myCustomHandlerFactory', function ($translate) {
  // has to return a function which gets a tranlation ID
      //console.log(dep1,dep2)
        return function (translationID) {
        // do something with dep1 and dep2
      };
});
    // localization service responsible for retrieving resource files from the server and
    // managing the translation dictionary
