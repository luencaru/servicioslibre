$(document).ready(function () {
    var cssefect = {
        register: function () {
            $("#prov").on("click", function () {
                $("#proveedor").css("display", "block");
                $("#servicio").css("display", "none");
            });
            $("#serv").on("click", function () {
                $("#servicio").css("display", "block");
                $("#proveedor").css("display", "none");
            });
        },
        desplazamiento: function () {
            $(".btn_down i").on("click", function () {
                setTimeout(function () {
                    $("html,body").animate({ scrollTop: $(".categorias").offset().top  }, 1200);
                }, 100)
            });
        }
    };
    cssefect.register();
    cssefect.desplazamiento();
});