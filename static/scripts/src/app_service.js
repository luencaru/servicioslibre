angular.module('project', ['ngRoute'])
    .config(function ($routeProvider, $interpolateProvider, $locationProvider) {

        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');

        var _origin = window.location.origin ;

        $routeProvider
            .when('/', {
                controller: 'subcategoryCtrl',
                template: $('#list_subcategory').html()
            })
            .otherwise({
                redirectTo: '/'
            });
    })

    .controller('subcategoryCtrl', function ($scope, $http) {

        $scope.sub_category = [
            { name: 'Elegir subcategoría', value: 0 }
        ];

        $scope.form_sub = $scope.sub_category[0].value;

        $scope.sc_event = {
            cat_state: false,
            show_service: function () {
                $("#info_service").fadeIn(200);
                $(".select_cat").fadeOut(200);
                $(".fn_begin").fadeIn(200);
                this.cat_state = true;
            },
            step_first: function () {
                $(".fsp_segundo").fadeOut(300);
                $(".sn_two").css({"background-color": "white", "color": "#51B1BE"});
                setTimeout(function () {
                    $(".fsp_primero").fadeIn(300);
                    $(".one_two").css("width", "0");
                    $(".rs_content").css("width", "570px");
                    $(".fs_paso .fsp_head").css("width", "600px");
                    $(".rs_content .fsp_head .fsph_cont .line_separ").css("width", "240px");
                }, 300);
            },
            step_two: function () {
                if ($scope.ser_type == undefined) {
                    $scope.ser_type_state = true;
                } else {
                    $scope.ser_type_state = false;
                }

                if ($scope.ser_begin == undefined) {
                    $scope.ser_begin_state = true;
                } else {
                    $scope.ser_begin_state = false;
                }

                if ($scope.nro_people == undefined || $scope.nro_people<0) {
                    $scope.nro_people_state = true;
                } else {
                    $scope.nro_people_state = false;
                }

                if ($scope.ser_date == undefined) {
                    $scope.ser_date_state = true;
                } else {
                    $scope.ser_date_state = false;
                }

                if ($scope.ser_time == undefined || $scope.ser_time<0) {
                    $scope.ser_time_state = true;
                } else {
                    $scope.ser_time_state = false;
                }

                if ($scope.ser_address == undefined) {
                    $scope.ser_address_state = true;
                } else {
                    $scope.ser_address_state = false;
                }

                if (this.cat_state == false) {
                    $(".select_cat").fadeIn(200);
                } else {
                    if ($scope.ser_type_state == false && $scope.nro_people_state == false && $scope.ser_date_state == false && $scope.ser_time_state == false && $scope.ser_address_state == false) {
                        var send_json = {
                            'subcategory': $scope.form_sub,
                            'details': {
                                'duration': $scope.ser_time,
                                'number_people': $scope.nro_people,
                                'event_type': $scope.ser_type
                            },
                            'deadline_date': $scope.ser_date,
                            'deadline_hour': $scope.ser_begin,
                            'address': $scope.ser_address
                        };
                        localStorage.setItem("service", JSON.stringify(send_json));
                        $(".rs_content").css("width", "800px");
                        $(".fs_paso .fsp_head").css("width", "830px");
                        $(".rs_content .fsp_head .fsph_cont .line_separ").css("width", "470px");
                        $(".select_cat").fadeOut(200);
                        $(".fsp_tercero").fadeOut(300);
                        $(".fsp_primero").fadeOut(300);
                        $(".sn_three").css({"background-color": "white", "color": "#51B1BE"});
                        $(".one_two").css("width", "40px");
                        setTimeout(function () {
                            $(".fsp_segundo").fadeIn(300);
                            $(".two_three").css("width", "0");
                        }, 300);
                        setTimeout(function () {
                            $(".sn_two").css({"background-color": "#51B1BE", "color": "white"});
                        }, 600);
                    }
                }
            }
        }
    });