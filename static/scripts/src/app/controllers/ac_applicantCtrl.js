/**
 * Created by rikhart on 8/14/14.
 */
function ac_applicantCtrl($scope, api, $state) {

    $scope.applicant = {
        data:null,
        name_service:null,
        _constructor: function() {
            var self = this;
            api.services.one($state.params.id).get().then(function(res){
                self.data= res;
                console.log(res,"res");
                $(".block_loading").css("display","none");
                api.services.one($state.params.id).get().then(function(res){
                    self.name_service = res;
                });
            });
        },
        go_chat: function (item) {
            $state.go('account.chat',{id: item.id});
        }
    };

    $scope.applicant._constructor();

}