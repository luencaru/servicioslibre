/**
 * Created by rikhart on 3/13/14.
 */
function ac_solicitudCtrl($scope, api, $state) {

    $scope.client = true;

    $scope.solicitud = {
        data: null,
        _constructor: function () {
            var self = this;

            api.me.one("services").get().then(function (res) {
                $(".block_loading").css("display","none");
                self.data = res.results;
            });

            if (localStorage.getItem("service") != null) {
                var data_local = localStorage.getItem("service"),
                    send_local = JSON.parse(data_local);
                api.services.customPOST({
                    address: send_local.address,
                    subcategory: send_local.subcategory,
                    details: {
                        duration: send_local.duration,
                        'number-people': send_local.number_people,
                        'event-type': send_local.event_type
                    },
                    deadline_date: send_local.deadline_date,
                    deadline_hour: send_local.deadline_hour
                }).then(function () {
                    localStorage.removeItem("service");
                    api.me.one("services").get().then(function (res) {
                        self.data = res.results;
                    });
                })
            }
        },
        go_list_applicant: function (item) {
            $state.go('account.request.list_applicant', {id: item.id});
        }
    };
    $scope.solicitud._constructor();

}