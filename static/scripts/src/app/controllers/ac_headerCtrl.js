/**
 * Created by rikhart on 3/13/14.
 */
function ac_headerCtrl($scope,api,$state) {

    $scope.client = true;

    $scope.header = {
        data:[],
        _constructor: function() {
            var self = this;
            api.me.one("notifications").get().then(function(res){
                self.data = res.results;
            });
        },
        go_service: function(id) {
            api.notifications.one(id).one("read").customPOST({
            }).then(function () {
                console.log("leido")
            });
            $state.go("chat", {id: id});
        }
    };

    $scope.header._constructor();

    var cssefect = {
        dropdown: function () {
            $(".du_dropdown").on("click", function () {
                $('.dropdown1').stop().slideToggle(200);
                $('.dropdown2').stop().slideUp(200);
                return false;
            });
            $(".du_notif").on("click", function(){
                $('.dropdown1').stop().slideUp(200);
                $('.dropdown2').stop().slideToggle(200);
                return false;
            });
            $('.wrapper').on("click", function () {
                $('.dropdown1').slideUp(200);
                $('.dropdown2').slideUp(200);
            });
        }
    };

    cssefect.dropdown();

    $(".dd_text").on("click", function (e) {
        e.stopPropagation();
    });
    $(".no_notific").on("click", function (e) {
        e.stopPropagation();
    });
}