/**
 * Created by rikhart on 8/12/14.
 */
function ac_createCtrl($scope, api, $state) {

    $scope.create = {
        category: [],
        _constructor: function () {
            var self = this;
            api.categories.get().then(function (res) {
                self.category = res;
            })
        }
    };

    $scope.create._constructor();

    $scope.category = [
        { name: 'Elegir categoría', value: 0 }
    ];

    $scope.create.choose_cat = $scope.category[0].value;

    $scope.sub_category = [
        { name: 'Elegir subcategoría', value: 0 }
    ];

    $scope.create.choose_sub = $scope.sub_category[0].value;

    $scope.events_complete = [
        "Boda",
        "Cumpleaños",
        "Quinceañero",
        "Fiesta",
        "Despedida de soltero",
        "Despedida de soltera",
        "Matrimonio",
        "Fiesta de quinceaños",
    ];

    $("#tags_event").autocomplete({
        source: $scope.events_complete
    });

    $scope.result1 = '';
    $scope.options1 = null;
    $scope.details1 = '';

    $scope.sc_event = {
        cat_state: false,
        show_subcategory: function () {
            $("#pre_loader").fadeIn(200);
            api.categories.one($scope.create.choose_cat).one("subcategories").get().then(function (res) {
                $scope.create.subcategories = res;
                api.districts.get().then(function (res) {
                    $scope.districts = res;
                    $(".chos_subcat").fadeIn(200);
                    $("#pre_loader").css("display", "none");
                });
            });
        },
        show_service: function () {
            $("#info_service").fadeIn(200);
            $(".select_cat").fadeOut(200);
            $(".fn_begin").fadeIn(200);
            this.cat_state = true;
        },
        step_first: function () {
            $(".fsp_segundo").fadeOut(300);
            $(".sn_two").css({"background-color": "white", "color": "#51B1BE"});
            setTimeout(function () {
                $(".fsp_primero").fadeIn(300);
                $(".one_two").css("width", "0");
            }, 300);
        },
        go_service: function () {
            $state.go('account.request');
        },
        step_two: function () {

            if ($("#district").val() == null) {
                $scope.main_dist = true;
            } else {
                $scope.main_dist = false;
                $scope.main_distrito = $("#district").val();
                for (var i = 0; i < $scope.districts.length; i++) {
                    if( $scope.main_distrito == $scope.districts[i].postal_code ) {
                        $scope.name_dist = $scope.districts[i].name;
                    }
                }
            }

            if ($scope.ser_type == undefined) {
                $scope.ser_type_state = true;
            } else {
                $scope.ser_type_state = false;
            }

            if ($scope.ser_begin == undefined) {
                $scope.ser_begin_state = true;
            } else {
                $scope.ser_begin_state = false;
            }

            if ($scope.nro_people == undefined || $scope.nro_people < 0) {
                $scope.nro_people_state = true;
            } else {
                $scope.nro_people_state = false;
            }

            if ($scope.ser_date == undefined) {
                $scope.ser_date_state = true;
            } else {
                $scope.ser_date_state = false;
            }

            if ($scope.ser_time == undefined || $scope.ser_time < 0) {
                $scope.ser_time_state = true;
            } else {
                $scope.ser_time_state = false;
            }

            if ($scope.ser_address == undefined) {
                $scope.ser_address_state = true;
            } else {
                $scope.ser_address_state = false;
            }

            if (this.cat_state == false) {
                $(".select_cat").fadeIn(200);
            } else {
                if ($scope.ser_type_state == false && $scope.nro_people_state == false && $scope.ser_date_state == false && $scope.ser_time_state == false && $scope.ser_address_state == false && $scope.main_dist == false) {
                    $scope.data_create = {
                        'subcategory': $scope.create.choose_sub,
                        'details': {
                            'duration': $scope.ser_time,
                            'number_people': $scope.nro_people,
                            'event_type': $scope.ser_type
                        },
                        'deadline_date': $scope.ser_date,
                        'deadline_hour': $scope.ser_begin,
                        'address': $scope.ser_address
                    };
                    $(".select_cat").fadeOut(200);
                    $(".fsp_tercero").fadeOut(300);
                    $(".fsp_primero").fadeOut(300);
                    $(".sn_three").css({"background-color": "white", "color": "#51B1BE"});
                    $(".one_two").css("width", "40px");
                    setTimeout(function () {
                        $(".fsp_segundo").fadeIn(300);
                        $(".two_three").css("width", "0");
                    }, 300);
                    setTimeout(function () {
                        $(".sn_two").css({"background-color": "#51B1BE", "color": "white"});
                    }, 600);
                }
            }
        },
        create_service: function () {
            $(".create_loading").fadeIn(200);
            api.services.customPOST({
                address: $scope.data_create.address,
                subcategory: $scope.data_create.subcategory,
                details: {
                    duration: $scope.data_create.duration,
                    'number-people': $scope.data_create.number_people,
                    'event-type': $scope.data_create.event_type
                },
                deadline_date: $scope.data_create.deadline_date,
                deadline_hour: $scope.data_create.deadline_hour,
                district: $scope.main_distrito
            }).then(function () {
                $(".btn_before").fadeOut(300);
                $(".create_loading").fadeOut(300);
                setTimeout(function () {
                    $(".btn_after").fadeIn(300);
                    $(".charge_service .cs_text").fadeIn(300);
                    $state.go('account.request');
                }, 300);
            })
        }
    };

}