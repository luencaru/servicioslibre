/**
 * Created by rikhart on 8/12/14.
 */
function ac_chatCtrl($scope, api, $state) {

    $scope.client = true;

    $scope.chat = {
        prof: false,
        _constructor: function () {
            api.applies.one($state.params.id).get().then(function(res){
                $scope.apply = res;
                $(".block_loading").css("display","none");
                $(".block_chat_prof").fadeIn(300);
            });
            $('.js-auto-size').textareaAutoSize();
            api.applies.one($state.params.id).one("messages").get().then(function (res) {
                $scope.messages = res.results;
            });
            setInterval(function () {
                api.applies.one($state.params.id).one("messages").get().then(function (res) {
                    $scope.messages = res.results;
                })
            }, 10000);
        },
        connect: function () {
            //api.applies.one($state.params.id).one("connect").customPOST({
            //}).then(function () {
                $scope.apply.connected = true;
                $(".first_aviso").slideUp(300);
            //});
        },
        send_price: function (form) {
            api.applies.one($state.params.id).customPUT({
                new_price: form.price.$modelValue
            }).then(function () {
            })
        },
        send_message: function (form) {
            if (form.content.$modelValue == undefined) {
                $(".wrap_message").css("border", "1px solid red")
            } else {
                api.messages.one("send").customPOST({
                    content: form.content.$modelValue,
                    apply: $state.params.id
                }).then(function () {
                    $("#text_message").val("");
                    api.applies.one($state.params.id).one("messages").get().then(function (res) {
                        $scope.messages = res.results;
                    })
                })
            }
        },
        accept_propuesta: function() {
            api.applies.one($state.params.id).customPUT({
                accepted: true
            }).then(function(){
            })
        },
        see_profile: function () {
            var self = this;
            if (self.prof != true) {
                self.prof = true;
                $("#block_chat").fadeOut(300);
                setTimeout(function () {
                    $("#block_profile").fadeIn(300);
                }, 300);
            }
        },
        see_chat: function () {
            var self = this;
            if (self.prof != false) {
                self.prof = false;
                $("#block_profile").fadeOut(300);
                setTimeout(function () {
                    $("#block_chat").fadeIn(300);
                }, 300);
            }
        }
    };


    $scope.chat._constructor();

    $scope.profile = {
        data: null,
        review: null,
        _constructor: function () {
            var self = this;
            api.applies.one($state.params.id).one("user").get().then(function (res) {
                self.data = res;
            });
            api.applies.one($state.params.id).one("user").one("reviews").get().then(function (res) {
                self.review = res;
            });
        },
        modal_img: function () {
            var alto = $(window).height();
            var ancho = $(window).width();
            $("#img_photo").css({"max-height": alto - $("#mod_desc").height() - 250 + "px", "max-width": ancho - 100 + "px"});
            $(window).resize(function () {
                var alto = $(window).height();
                var ancho = $(window).width();
                $("#img_photo").css({"max-height": alto - $("#mod_desc").height() - 250 + "px", "max-width": ancho - 100 + "px"})
            })
        },
        stop_prog: function () {
            $(".up_data_wkp").on("click", function (e) {
                e.stopPropagation();
            });
            $(".show_photo_wk").on("click", function (e) {
                e.stopPropagation();
            });
            $(".show_avl").on("click", function (e) {
                e.stopPropagation();
            });
        },
        close_modal: function () {
            $(".up_data_wkp").fadeOut(200);
            $(".show_photo_wk").fadeOut(200);
            $(".back_shadow").fadeOut(400);
            this.state = false;
        },
        wrap_close: function () {
            var self = this;
            $(document).keydown(function (e) {
                if (e.keyCode === 27) {
                    if (self.state == true) {
                        if ($scope.edit == true) {
                            $scope.myCalendar.fullCalendar('removeEvents');
                            $scope.origin_array.forEach(function (obj) {
                                $scope.myCalendar.fullCalendar("addEventSource", [obj]);
                            });
                        }
                        $(".up_data_wkp").fadeOut(200);
                        $(".show_photo_wk").fadeOut(200);
                        $(".back_shadow").fadeOut(400);
                        $(".show_avl").fadeOut(400);
                        self.state = false;
                    }
                }
            });
            $(".wrapper").on("click", function () {
                if (self.state == true) {
                    if ($scope.edit == true) {
                        $scope.myCalendar.fullCalendar('removeEvents');
                        $scope.origin_array.forEach(function (obj) {
                            $scope.myCalendar.fullCalendar("addEventSource", [obj]);
                        });
                    }
                    $(".up_data_wkp").fadeOut(200);
                    $(".show_photo_wk").fadeOut(200);
                    $(".back_shadow").fadeOut(400);
                    $(".show_avl").fadeOut(400);
                    self.state = false;
                }
            });
        },
        show_photo_wk: function (item) {
            $scope.photo_selected = item;
            var self = this;
            $(".back_shadow").fadeIn(200);
            $(".show_photo_wk").fadeIn(400, function () {
                $scope.$emit("art_center");
            });
            setTimeout(function () {
                self.state = true;
            }, 100);
        },
        back_shadow: function () {
            var height = $(window).height();
            $(".back_shadow").css("height", height + "px");
            $(window).resize(function () {
                var height = $(window).height();
                $(".back_shadow").css("height", height + "px");
            });
        }
    };

    $scope.profile._constructor();
    $scope.profile.back_shadow();
    $scope.profile.wrap_close();
    $scope.profile.stop_prog();

}