var App = angular.module('suricats', ['ngAnimate', 'ngMockE2E' , 'ngTagsInput', 'ngRoute' , 'restangular', 'ui.autocomplete', 'ngAutocomplete', 'ui.router', 'infinite-scroll', 'ngResource', 'angularFileUpload', 'ngCookies']).provider('$cookie', function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    this.$get = function () {
        return {
            csrftoken: getCookie('csrftoken'),
            rol: getCookie('rol')
        }
    }
}).config(['$routeProvider', '$urlRouterProvider', '$httpProvider', '$stateProvider', '$interpolateProvider', '$cookieProvider', '$locationProvider', '$provide', 'RestangularProvider', function ($routeProvider, $urlRouterProvider, $httpProvider, $stateProvider, $interpolateProvider, $cookieProvider, $locationProvider, $provide, RestangularProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    //$locationProvider.hashPrefix('app!');//Mientrastanto
    $locationProvider
        .html5Mode(true)
        .hashPrefix('!');

    //$locationProvider.html5Mode(true);
    var cookie = $cookieProvider.$get()

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn’t scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                xhr.setRequestHeader("X-CSRFToken", cookie.csrftoken);
            }
        }
    });
    /*UNRESTANGULARIZE OBJECT*/
    RestangularProvider.setResponseExtractor(function (response) {
        var newResponse = response;
        if (angular.isArray(response)) {
            angular.forEach(newResponse, function (value, key) {
                newResponse[key].originalElement = angular.copy(value);
            });
        } else {
            newResponse.originalElement = angular.copy(response);
        }
        return newResponse;
    });
    $httpProvider.defaults.headers.common['X-CSRFToken'] = cookie.csrftoken;

    $provide.decorator('$httpBackend', function ($delegate) {
        var proxy = function (method, url, data, callback, headers) {
            var interceptor = function () {
                var _this = this,

                    _arguments = arguments;
                setTimeout(function () {
                    callback.apply(_this, _arguments);
                }, 800);
            };
            return $delegate.call(this, method, url, data, interceptor, headers);
        };
        for (var key in $delegate) {
            proxy[key] = $delegate[key];
        }
        return proxy;
    });


    var _origin = window.location.origin + '/';
    var root = {
        name: 'root',
        url: '',
        abstract: true
    };

    var account = {
        name: "account",
        url: '',
        abstract: true,
        parent: "root",
        views: {
            "header@": {
                templateUrl: _origin + "partials/header_log.html",
                controller: "ac_headerCtrl"
            },
            "footer@": {
                templateUrl: _origin + "partials/footer.html"
            }
        }
    };
    var request = {
        name: "account.request",
        url: "/servicios",
        parent: "account",
        views: {
            "@": {
                templateUrl: _origin + "partials/solicitudes.html",
                controller: "ac_solicitudCtrl"
            }
        }
    };

    var list_applicant = {
        name: "account.request.list_applicant",
        url: "/:id/propuesta",
        parent: "account.request",
        resolve: {
            promise: function ($stateParams) {
                return $stateParams.id;
            }
        },
        views: {
            "@": {
                templateUrl: _origin + "partials/list_applicant.html",
                controller: "ac_applicantCtrl"
            }
        }
    };

    var create = {
        name: "account.create",
        url: "/solicitar",
        parent: "account",
        views: {
            "@": {
                templateUrl: _origin + "partials/create_service.html",
                controller: "ac_createCtrl"
            }
        }
    };

    var chat = {
        name: "account.chat",
        url: "/propuesta/:id/",
        parent: "account",
        resolve: {
             promise: function ($stateParams) {
                 return $stateParams.id;
             }
         },
        views: {
            "@": {
                 templateUrl: _origin + "partials/conectado.html",
                 controller: "ac_chatCtrl"
             }
        }
    };

    $stateProvider.state(root).state(account).state(request).state(create).state(list_applicant).state(chat);
    $urlRouterProvider.otherwise('/servicios');

}]).run(['$rootScope', '$state', '$stateParams', 'Restangular', '$httpBackend', function ($rootScope, $state, $stateParams, Restangular, $httpBackend) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    //$state.transitionTo('account.init')
    Restangular.setBaseUrl('/api');
    Restangular.setRequestSuffix('/');

    //Backend:

    $httpBackend.whenGET(new RegExp('partials/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/me/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/services/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/services/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/categories/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/applies/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/applies/.*')).passThrough();
    $httpBackend.whenPUT(new RegExp('/api/applies/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/messages/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/notifications/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/districts/.*')).passThrough();

    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);


App.factory('api', function (Restangular) {
    return {
        me: Restangular.one('me'),
        categories: Restangular.one('categories'),
        services: Restangular.one('services'),
        applies: Restangular.one('applies'),
        notifications: Restangular.one('notifications'),
        messages: Restangular.one('messages'),
        districts: Restangular.one('districts')
    }
});

App.directive('promStar', function () {
    return {
        restrict: 'A',
        scope: {
            total: '=',
            rated: '='
        },
        link: function (scope, element) {
            function promedio() {
                var prom = scope.total / scope.rated;
                if (scope.rated != 0) {
                    element.append(prom)
                } else {
                    element.append("0")
                }
            }

            scope.$watch("total", function () {
                promedio()
            })
        }
    }
});

App.directive('widthColor', function () {
    return {
        restric: 'A',
        scope: {
            totalstars: '=',
            ratedservices: '='
        },
        link: function (scope, element) {
            function colorbarra() {
                var ancho = scope.totalstars / scope.ratedservices;
                var color = 23 * ancho;
                if (scope.ratedservices == 0) {
                    element.css('width', '0px');
                } else {
                    element.css('width', color + 'px');
                }
            }

            scope.$watch("totalstars", function () {
                colorbarra()
            })
        }
    }
});

App.directive('artSlider', function ($timeout) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            element.addClass("art-slider")
            var array = [];
            scope.$watch(attrs.artdata, function (sew, old) {
                if (sew) {
                    array = sew;
                }
            });
            var prev = element.find(".prev");
            var next = element.find(".next");
            next.bind("click", function () {
                var first = array[0];
                first.first = "af-true";
                scope.$apply();
                $timeout(function () {
                    var top = array.shift();
                    array.push(top);
                    $timeout(function () {
                        first.first = null;
                    })
                }, 500)
            });
            prev.bind("click", function () {
                var last = array[0];
                last.first = "al-true";
                scope.$apply();
                $timeout(function () {
                    var top = array.pop();
                    array.unshift(top);
                    $timeout(function () {
                        last.first = null;
                    })
                }, 500)
            });
        }
    }
});

App.directive('imgFacebook', function () {
    return {
        restrict: "A",
        scope: {
            src: '='
        },
        link: function (scope, element, attrs) {
            var image = element;

            function size_image(img) {
                if (img.width() > img.height()) {
                    element.css({"height": "110px", "width": "100%"})
                } else {
                    element.css({"height": "100%", "width": "152.5px"})
                }
            }

            scope.$watch("src", function () {
                size_image(image)
            })
        }
    }
});

App.directive('center', function () {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            function center() {
                element.css({
                    "position": "fixed",
                    "z-index": "111",
                    "top": $(window).height() / 2 - element.height() / 2,
                    "left": $(window).width() / 2 - element.width() / 2
                });
            }

            center();
            scope.$watch(function () {
                return element.height();
            }, function () {
                scope.$evalAsync(function () {
                    center();
                });
            });
            $(window).resize(function () {
                center();
            });
            scope.$on("art_center", function (e) {
                center();
            })
        }
    }
});