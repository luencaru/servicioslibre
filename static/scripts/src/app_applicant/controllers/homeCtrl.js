/**
 * Created by rikhart on 7/4/14.
 */
App.controller("homeCtrl", function ac_headerCtrl($scope, $state, api) {

    $scope.ac_home = {
        data: null,
        _constructor: function () {
            var self = this;
            api.me.one("services-home").get().then(function (res) {
                self.data = res;
                $(".block_loading").css("display","none");
            });
        },
        modal: function (service) {
            $scope.$emit("SEND_DATA", service);
        },
        categ_g: function () {
            $state.go('category_g');
        },
        see_more: function (service) {
            var subcategory = service.data[0].subcategory;
            $state.go('subcategory', {name: subcategory});
            $scope.$emit("NAME_SUBCATEGORY", subcategory);
        },
        go_profile: function () {
            $state.go('profile');
        }
    };

    $scope.ac_home._constructor();


});