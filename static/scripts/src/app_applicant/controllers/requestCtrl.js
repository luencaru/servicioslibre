/**
 * Created by carlos on 08/07/14.
 */
App.controller("requestCtrl", function ac_headerCtrl($scope, $state, api) {

    $scope.applicant = true;

    $scope.ac_request = {
        data: null,
        _constructor: function () {
            var self = this;
            api.me.one("applies").get().then(function (res) {
                self.data = res.results;
                $(".block_loading").css("display","none");
            });
        },
        go_chat: function (item) {
            $state.go('chat', {id: item.id});
        }
    };
    $scope.ac_request._constructor();

});