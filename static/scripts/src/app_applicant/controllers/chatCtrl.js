/**
 * Created by carlos on 08/07/14.
 */
App.controller("chatCtrl", function ac_headerCtrl($scope, $state, api) {

    $scope.s_price = null;
    $scope.applicant = true;

    $scope.chat = {
        _constructor: function () {
            api.applies.one($state.params.id).get().then(function (res) {
                $scope.apply = res;
                $(".block_loading").css("display","none");
                $(".block_chat_prof").fadeIn(300);
            });
            api.applies.one($state.params.id).one("messages").get().then(function(res){
                $scope.messages=res.results;
            });
            setInterval(function(){
                api.applies.one($state.params.id).one("messages").get().then(function(res){
                    $scope.messages=res.results;
                })
            },10000);
            $('.js-auto-size').textareaAutoSize();
        },
        send_price: function (form) {
            api.applies.one($state.params.id).customPUT({
                new_price: form.price.$modelValue
            }).then(function(){
            })
        },
        accept_propuesta: function() {
            api.applies.one($state.params.id).customPUT({
                accepted: true
            }).then(function(){
            })
        },
        send_message: function(form) {
            if(form.content.$modelValue==undefined) {
                $(".wrap_message").css("border","1px solid red")
            } else {
                api.messages.one("send").customPOST({
                    content: form.content.$modelValue,
                    apply: $state.params.id
                }).then(function() {
                    $("#text_message").val("");
                })
            }
        }
    };

    $scope.chat._constructor();

});