/**
 * Created by carlos on 10/07/14.
 */
App.controller("first_stepCtrl", function ac_headerCtrl($scope, $state, api) {

    $scope.events = [];
    //Calendario:
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 1300,
            editable: true,
            selectable: true,
            allDaySlot: false,
            slotMinutes: 60,
            header: {
                left: false,
                right: false,
                center: false
            },
            defaultView: 'agendaWeek',
            select: function (startDate, endDate, allDay, jsEvent, view) {
                var start_date = new Date(startDate),
                    event = [
                        {title: '', start: startDate, end: endDate, allDay: false, id: start_date.getDay()}
                    ],
                    array = $scope.myCalendar.fullCalendar('clientEvents');
                for (var j = 0; j < array.length; j++) {
                    if (array[j].id == event[0].id) {
                        array.splice(j, 1);
                        $scope.myCalendar.fullCalendar('removeEventSource', array[j]);
                        break;
                    }
                }
                $scope.myCalendar.fullCalendar('addEventSource', event);
            },
            eventRender: function (event, element, view) {
                var array = $scope.myCalendar.fullCalendar('clientEvents');
                element.attr("day", event.id);
                element.bind("click", function (e) {
                    var id = $(this).attr("day");
                    for (var i = 0; i < array.length; i++) {
                        if (id == array[i].id) {
                            array.splice(i, 1);
                            $scope.myCalendar.fullCalendar('removeEventSource', array[i]);
                        }
                    }
                });
                element.prepend("<a class='cal_btn_delete' data-event='" + event.id + "'>Borrar <i class='fa fa-times'></i></a>");
            }
        }
    };

    $scope.eventSources = [$scope.events, []];

    Array.prototype.equals = function (array) {
        if (!array)
            return false;

        if (this.length != array.length)
            return false;

        for (var i = 0, l = this.length; i < l; i++) {
            if (this[i] instanceof Array && array[i] instanceof Array) {
                if (!this[i].equals(array[i]))
                    return false;
            }
            else if (this[i] != array[i]) {
                return false;
            }
        }
        return true;
    };

    $scope.f_step = {
        data: null,
        subcategory: null,
        category: null,
        select: [],
        photo: null,
        sub_user: null,
        _constructor: function () {
            var self = this;
            api.me.get().then(function (res) {
                self.data = res;
                self.category = res.category;
            });
        },
        to_array: function (array) {
            var d_array = [],
                day = null;
            for (var j = 0; j < array.length; j++) {
                var start_date = array[j].start.toString(),
                    stop = false,
                    i = 0,
                    current_day = [];
                while (i < start_date.length && stop == false) {
                    if (start_date[i + 1].charCodeAt(0) == 32) {
                        stop = true;
                    }
                    current_day.push(start_date[i]);
                    i = i + 1;
                }

                var t = Date.parse(array[j].start),
                    e = Date.parse(array[j].end),
                    d = new Date(t),
                    f = new Date(e),
                    hour_star = d.getHours(),
                    hour_end = f.getHours();

                if (current_day.equals(["S", "u", "n"])) {
                    day = "sunday";
                }
                if (current_day.equals(["M", "o", "n"])) {
                    day = "monday";
                }
                if (current_day.equals(["T", "u", "e"])) {
                    day = "tuesday";
                }
                if (current_day.equals(["W", "e", "d"])) {
                    day = "wednesday";
                }
                if (current_day.equals(["T", "h", "u"])) {
                    day = "thursday";
                }
                if (current_day.equals(["F", "r", "i"])) {
                    day = "friday";
                }
                if (current_day.equals(["S", "a", "t"])) {
                    day = "saturday";
                }
                d_array.push({"day": day, "since": hour_star, "to": hour_end})
            }
            return d_array;
        }
    };
    $scope.f_step._constructor();

    $scope.cant_n = 1;
    $scope.cant_dist = 1;
    $scope.json_numbers = {};
    $scope.array_district = [];

    $scope.c_event = {
        state_select: false,
        choose_cat: null,
        category: null,
        subcategories: null,
        step_first: function () {
            $(".fsp_segundo").fadeOut(300);
            $(".sn_two").css({"background-color": "white", "color": "#51B1BE"})
            setTimeout(function () {
                $(".fsp_primero").fadeIn(300);
                $(".one_two").css("width", "0");
            }, 300);
        },
        step_two: function (first_s) {

            var self = this;

            api.districts.get().then(function (res) {
                $scope.districts = res;
            });

            $('.js-auto-size').textareaAutoSize();

            for (var i = 1; i < $scope.cant_n; i++) {
                if ($("#operador" + i).val() != "" && $("#numero" + i).val() == "") {
                    $(".number" + i).addClass("fs_valid");
                    $(".operator" + i).removeClass("fs_valid");
                }
                if ($("#operador" + i).val() == "" && $("#numero" + i).val() != "") {
                    $(".number" + i).removeClass("fs_valid");
                    $(".operator" + i).addClass("fs_valid");
                }
                if ($("#operador" + i).val() != "" && $("#numero" + i).val() != "") {
                    $scope.json_numbers[$("#operador" + i).val()] = $("#numero" + i).val();
                }
            }

            if (first_s.name.$modelValue == undefined) {
                $scope.f_name = true;
            } else {
                $scope.f_name = false;
            }

            if (first_s.last_name.$modelValue == undefined) {
                $scope.l_name = true;
            } else {
                $scope.l_name = false;
            }

            if (first_s.dni.$modelValue == undefined) {
                $scope.l_dni = true;
            } else {
                $scope.l_dni = false;
            }

            if (first_s.landline.$modelValue == undefined) {
                $scope.l_line = true;
            } else {
                $scope.l_line = false;
            }

            if (first_s.mobil_phone.$modelValue == undefined) {
                $scope.m_phone = true;
            } else {
                $scope.m_phone = false;
            }

            if (first_s.dni.$modelValue != undefined && first_s.name.$modelValue != undefined && first_s.last_name.$modelValue != undefined && first_s.landline.$modelValue != undefined && first_s.mobil_phone.$modelValue != undefined) {
                $(".fsp_primero").fadeOut(300);
                $(".fsp_tercero").fadeOut(300);
                $(".sn_three").css({"background-color": "white", "color": "#51B1BE"})
                $(".one_two").css("width", "40px");
                setTimeout(function () {
                    $(".fsp_segundo").fadeIn(300);
                    $(".two_three").css("width", "0");
                }, 300);
                setTimeout(function () {
                    $(".sn_two").css({"background-color": "#51B1BE", "color": "white"})
                }, 600);
            }
        },
        step_three: function (first_s) {
            var self = this,
                dist = 0;
            if($("#district").val()==null) {
                $scope.main_dist=true;
            }else {
                $scope.main_dist=false;
                $scope.main_distrito=$("#district").val();
            }
            for (var i = 0; i < $scope.cant_dist; i++) {
                var id = $("#district" + i).val();
                if (id != null) {
                    $scope.array_district[dist] = id;
                    dist = dist + 1;
                }
            }

            api.categories.get().then(function (res) {
                self.choose_cat = res[0];
                self.category = res;
                api.categories.one(self.choose_cat.slug).one("subcategories").get().then(function (sub) {
                    self.subcategories = sub;
                })
            });

            if (this.photo == null) {
                $scope.pho_val = true;
            } else {
                $scope.pho_val = false;
            }

            if (first_s.address.$modelValue == undefined || first_s.address.$modelValue == "") {
                $scope.f_address = true;
            } else {
                $scope.f_address = false;
            }

            if ($scope.f_address == false && $scope.pho_val == false && $scope.main_dist == false) {
                $(".fsp_segundo").fadeOut(300);
                $(".fsp_cuarto").fadeOut(300);
                $(".sn_four").css({"background-color": "white", "color": "#51B1BE"})
                $(".two_three").css("width", "40px");
                setTimeout(function () {
                    $(".fsp_tercero").fadeIn(300);
                    $(".three_four").css("width", "0");
                }, 300);
                setTimeout(function () {
                    $(".sn_three").css({"background-color": "#51B1BE", "color": "white"})
                }, 600);
            }
        },
        step_four: function (first_s) {
            $(".select_cat").fadeOut(200);
            $scope.select = $.map($(".i_subcat:checked"), function (item) {
                return $(item).val();
        });

            if ($scope.select.length == 0) {
                $(".fsp_error").fadeIn(200);
            } else {
                $(".fsp_error").fadeOut(200);
            }

            if ($scope.select.length != 0) {
                $(".fsp_tercero").fadeOut(300);
                $(".three_four").css("width", "40px");
                setTimeout(function () {
                    $(".fsp_cuarto").fadeIn(300);
                    $scope.myCalendar.fullCalendar('render');
                }, 300);
                setTimeout(function () {
                    $(".sn_four").css({"background-color": "#51B1BE", "color": "white"})
                }, 600);
            }
        },
        send_form: function (form) {

            var self = this;

            var data_days = $scope.f_step.to_array($scope.myCalendar.fullCalendar('clientEvents'));

            if (data_days.length == 0) {
                $(".fsp_error_avail").fadeIn(200);
            } else {
                $(".fsp_error_avail").fadeOut(200);
            }

            if (data_days.length != 0) {
                var data = new FormData();
                data.append('photo', this.photo);
                api.me.withHttpConfig({transformRequest: angular.identity}).customPOST(data, "profile-photo", undefined, {'Content-Type': undefined}).then(function (res) {
                    api.f_step.customPUT({
                        first_name: form.name.$modelValue,
                        dni: form.dni.$modelValue,
                        last_name: form.last_name.$modelValue,
                        mobile_phone: form.mobil_phone.$modelValue,
                        home_phone: form.landline.$modelValue,
                        address: form.address.$modelValue,
                        about_me: form.about.$modelValue,
                        other_phones: $scope.json_numbers,
                        subcategories: $scope.select,
                        district: $scope.main_distrito,
                        allow_districts: $scope.array_district,
                        availability: data_days
                    }).then(function () {
                        location.reload()
                    });
                });
            }
        },
        show_new_photo: function () {
            $('#in_n_photo').change(function (e) {
                addImage(e);
            });

            function addImage(e) {
                var file = e.target.files[0],
                    imageType = /image.*/;

                if (!file.type.match(imageType))
                    return;

                var reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
            }

            function fileOnload(e) {
                var result = e.target.result;
                $('#fs_ph_show').attr("src", result);
            }
        },
        add_numbers: function () {
            var self = this;
            $("#anc_agregar").append(
                "<div class='fp_block show_fadein'>" +
                    "<div class='fi_border operator" + $scope.cant_n + " anc_type'>" +
                    "<p class='fib_placeholder fibp_inline'>Operador</p>" +
                    "<input class='fib_input' maxlength='11' name='operador" + $scope.cant_n + "' type='text' id='operador" + $scope.cant_n + "'/>" +
                    "</div>" +
                    "<div class='fi_border number" + $scope.cant_n + " anc_number'>" +
                    "<p class='fib_placeholder fibp_inline'>Número</p>" +
                    "<input class='fib_input' maxlength='11' name='numero" + $scope.cant_n + "' type='text' id='numero" + $scope.cant_n + "'/>" +
                    "</div>" +
                    "</div>"
            );
            $scope.cant_n = $scope.cant_n + 1;
        },
        add_district: function () {
            var self = this,
                dist = null;
            for (var i = 0; i < $scope.districts.length; i++) {
                dist = dist + "<option value='" + $scope.districts[i].postal_code + "'>" + $scope.districts[i].name + "</option>";
            }
            $("#ad_agregar").append(
                "<div class='fi_border show_fadein'>" +
                    "<select class='fib_select' id='district" + $scope.cant_dist + "'>" +
                    "<option value='null'>Seleccione distrito</option>" + dist +
                    "</select>" +
                    "</div>"
            );
            $scope.cant_dist = $scope.cant_dist + 1;
        }
    };

    $scope.c_event.show_new_photo();

});
