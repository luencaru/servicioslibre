/**
 * Created by carlos on 08/07/14.
 */
App.controller("profileCtrl", function profileCtrl($scope, $state, api) {

    $scope.origin_array = [];
    $scope.events = [];
    $scope.edit = false;
    //Calendario:
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    $scope.week = [];

    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 1300,
            width: 500,
            selectable: true,
            allDaySlot: false,
            editable: true,
            slotMinutes: 60,
            header: {
                left: false,
                right: false,
                center: false
            },
            defaultView: 'agendaWeek',
            select: function (startDate, endDate, allDay, jsEvent, view) {
                $scope.edit = true;
                var start_date = new Date(startDate),
                    event = [
                        {title: '', start: startDate, end: endDate, allDay: false, id: start_date.getDay() }
                    ],
                    array = $scope.myCalendar.fullCalendar('clientEvents');
                for (var j = 0; j < array.length; j++) {
                    if (array[j].id == event[0].id) {
                        array.splice(j, 1);
                        $scope.myCalendar.fullCalendar('removeEventSource', array[j]);
                        break;
                    }
                }
                $scope.myCalendar.fullCalendar('addEventSource', event);
            },
            viewRender: function (view, element) {
                var start = new Date(view.visStart);
                var week = {};
                for (var i = 0; i < 7; i++) {
                    week[i] = {
                        date: new Date(start.getTime() + i * 24 * 60 * 60 * 1000)
                    };
                }
                $scope.week_prof.week = week;
            },
            eventRender: function (event, element, view) {
                var array = $scope.myCalendar.fullCalendar('clientEvents');
                element.attr("day", event.id);
                element.bind("click", function (e) {
                    var id = $(this).attr("day");
                    for (var i = 0; i < array.length; i++) {
                        if (id == array[i].id) {
                            array.splice(i, 1);
                            $scope.myCalendar.fullCalendar('removeEventSource', array[i]);
                        }
                    }
                });
                element.prepend("<a class='cal_btn_delete' data-event='" + event.id + "'>Borrar <i class='fa fa-times'></i></a>");
            }
        }
    };

    $scope.e_Sources = [$scope.events, []];

    Array.prototype.equals = function (array) {
        if (!array)
            return false;

        if (this.length != array.length)
            return false;

        for (var i = 0, l = this.length; i < l; i++) {
            if (this[i] instanceof Array && array[i] instanceof Array) {
                if (!this[i].equals(array[i]))
                    return false;
            }
            else if (this[i] != array[i]) {
                return false;
            }
        }
        return true;
    };

    $scope.week_prof = {
        availability: [],
        set_av: function (data) {
            var self = this;
            self.availability = data;
        },
        to_array: function (array) {
            var d_array = [],
                day = null;
            for (var j = 0; j < array.length; j++) {
                var start_date = array[j].start.toString(),
                    stop = false,
                    i = 0,
                    current_day = [];
                while (i < start_date.length && stop == false) {
                    if (start_date[i + 1].charCodeAt(0) == 32) {
                        stop = true;
                    }
                    current_day.push(start_date[i]);
                    i = i + 1;
                }

                var t = Date.parse(array[j].start),
                    e = Date.parse(array[j].end),
                    d = new Date(t),
                    f = new Date(e),
                    hour_star = d.getHours(),
                    hour_end = f.getHours();

                if (current_day.equals(["S", "u", "n"])) {
                    day = "sunday";
                }
                if (current_day.equals(["M", "o", "n"])) {
                    day = "monday";
                }
                if (current_day.equals(["T", "u", "e"])) {
                    day = "tuesday";
                }
                if (current_day.equals(["W", "e", "d"])) {
                    day = "wednesday";
                }
                if (current_day.equals(["T", "h", "u"])) {
                    day = "thursday";
                }
                if (current_day.equals(["F", "r", "i"])) {
                    day = "friday";
                }
                if (current_day.equals(["S", "a", "t"])) {
                    day = "saturday";
                }
                d_array.push({"day": day, "since": hour_star, "to": hour_end})
            }
            return d_array;
        },
        initialize: function () {
            var self = this;
            $scope.$watch("week_prof.week", function (sew, old) {
                if (sew) {
                    self.availability.forEach(function (obj) {
                        switch (obj.day) {
                            case "sunday":
                                var day = self.week[0].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                            case "monday":
                                var day = self.week[1].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                            case "tuesday":
                                var day = self.week[2].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                            case "wednesday":
                                var day = self.week[3].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                            case "thursday":
                                var day = self.week[4].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                            case "friday":
                                var day = self.week[5].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                            case "saturday":
                                var day = self.week[6].date;
                                obj.since = obj.since.split(":")[0];
                                obj.to = obj.to.split(":")[0];
                                var event = [
                                    {start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.since)), end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), parseInt(obj.to)), allDay: false, id: day.getDay() }
                                ]
                                $scope.myCalendar.fullCalendar("addEventSource", event);
                                break;
                        }
                    });
                    var origin_array = $scope.myCalendar.fullCalendar('clientEvents');
                    origin_array.forEach(function (obj) {
                        $scope.origin_array.push(obj);
                    });
                }
            });
        }
    };

    $scope.week_prof.initialize();

    $scope.aplicante = true;
    $scope.profile = {
        data: null,
        review: null,
        editingPhoto: false,
        subcategories: null,
        about_me: false,
        category: null,
        edit_sub: false,
        choose_sub: null,
        page: 1,
        select: [],
        state: false,
        length_ph: null,
        photo_selected: null,
        avail_state: false,
        _constructor: function () {
            var self = this;
            api.me.get().then(function (res) {
                self.data = res;
                self.length_ph = res.work_photos.length;
                self.category = res.category;
                $(".block_loading").css("display","none");
                $(".block_prof").fadeIn(200);
                api.categories.one(self.category).one("subcategories").get().then(function (res) {
                    self.subcategories = res;
                    self.choose_sub = self.subcategories[0];
                })
            });
            api.me.one("availability").get().then(function (res) {
                $scope.week_prof.set_av(res);
            });
        },
        send_avail: function () {
            var self = this;
            var data_days = $scope.week_prof.to_array($scope.myCalendar.fullCalendar('clientEvents')),
                up_array = $scope.myCalendar.fullCalendar('clientEvents');
            api.availability.customPUT({
                availability: data_days
            }).then(function () {
                $scope.origin_array = [];
                up_array.forEach(function (obj) {
                    $scope.origin_array.push(obj);
                });
                $scope.edit = false;
                $(".back_shadow").fadeOut(400);
                $(".show_avl").fadeOut(200);
                self.state = false;
            });
        },
        _reviews: function () {
            var self = this;
            api.me.one("reviews").get().then(function (res) {
                self.review = res;
            })
        },
        show_more_review: function () {
            var self = this;
            self.page = self.page + 1;
            api.me.one("reviews").get(['?page=', self.page]).then(function (res) {
            });
        },
        show_availability: function () {
            var self = this;
            $(".back_shadow").fadeIn(200);
            $(".show_avl").fadeIn(400);
            if (self.avail_state == false) {
                $scope.myCalendar.fullCalendar('render');
                self.avail_state = true;
            }
            setTimeout(function () {
                self.state = true;
            }, 10);
        },
        close_avail: function () {
            if ($scope.edit == true) {
                $scope.myCalendar.fullCalendar('removeEvents');
                $scope.origin_array.forEach(function (obj) {
                    $scope.myCalendar.fullCalendar("addEventSource", [obj]);
                });
            }
            $(".back_shadow").fadeOut(400);
            $(".show_avl").fadeOut(200);
            this.state = false;
        },
        change_subcategory: function () {
            this.edit_sub = true;
        },
        cancel_subcategory: function () {
            this.edit_sub = false;
        },
        update_subcategory: function (form) {
            var self = this;
            self.select = $.map($(".pro_sub:checked"), function (item) {
                return parseInt($(item).val());
            });
            api.f_step.customPUT({
                subcategories: self.select
            }).then(function () {
                self.edit_sub = false;
                $scope.profile._constructor();
            });
        },
        change_about_me: function () {
            this.about_me = true;
            $('.am_textarea').trigger('autosize.resize');
        },
        cancel_about_me: function () {
            this.about_me = false;
        },
        activateEditingPhoto: function () {
            this.editingPhoto = true;
        },
        cancelEditingPhoto: function () {
            this.editingPhoto = false;
        },
        update_photo: function () {
            var self = this;
            if (this.photo) {
                var data = new FormData();
                data.append('photo', this.photo);
                api.me.withHttpConfig({transformRequest: angular.identity}).customPOST(data, "profile-photo", undefined, {'Content-Type': undefined}).then(function (res) {
                    self.data.photo = res.photo;
                    self.editingPhoto = false;
                });
            }
        },
        modal_update: function () {
            var self = this;
            $(".back_shadow").fadeIn(200);
            $(".up_data_wkp").fadeIn(400);
            setTimeout(function () {
                self.state = true;
            }, 100);
        },
        back_shadow: function () {
            var height = $(window).height();
            $(".back_shadow").css("height", height + "px");
            $(window).resize(function () {
                var height = $(window).height();
                $(".back_shadow").css("height", height + "px");
            });
        },
        update_about: function (form) {
            var self = this;
            api.f_step.customPUT({
                about_me: form.about_me.$modelValue
            }).then(function () {
                self.about_me = false;
            });
        },
        update_works_photo: function (form) {
            var self = this;
            form.photo.$dirty = true;
            if (form.$valid) {
                if (this.w_photo) {
                    var data = new FormData();
                    data.append('image', this.w_photo);
                    data.append('subcategory', form.subcategory.$modelValue.id);
                    data.append('caption', form.description.$modelValue);
                    api.me.withHttpConfig({transformRequest: angular.identity}).customPOST(data, "work-photos", undefined, {'Content-Type': undefined}).then(function (res) {
                        self.data.work_photos.push(res);
                        self.length_ph = self.data.work_photos.length;
                        $(".update_wkp").fadeOut(200);
                        $(".back_shadow").fadeOut(400);
                        self.state = false;
                        $scope.profile.slider();
                    });
                }
            }
        },
        show_photo_wk: function (item) {
            $scope.photo_selected = item;
            var self = this;
            $(".back_shadow").fadeIn(200);
            $(".show_photo_wk").fadeIn(400, function () {
                $scope.$emit("art_center");
            });
            setTimeout(function () {
                self.state = true;
            }, 100);
        },
        show_new_photo: function (input, img) {
            $(input).change(function (e) {
                addImage(e);
            });

            function addImage(e) {
                var file = e.target.files[0],
                    imageType = /image.*/;

                if (!file.type.match(imageType))
                    return;

                var reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
            }

            function fileOnload(e) {
                var result = e.target.result;
                $(img).attr("src", result);
            }
        },
        close_modal: function () {
            $(".up_data_wkp").fadeOut(200);
            $(".show_photo_wk").fadeOut(200);
            $(".back_shadow").fadeOut(400);
            this.state = false;
        },
        wrap_close: function () {
            var self = this;
            $(document).keydown(function (e) {
                if (e.keyCode === 27) {
                    if (self.state == true) {
                        if ($scope.edit == true) {
                            $scope.myCalendar.fullCalendar('removeEvents');
                            $scope.origin_array.forEach(function (obj) {
                                $scope.myCalendar.fullCalendar("addEventSource", [obj]);
                            });
                        }
                        $(".up_data_wkp").fadeOut(200);
                        $(".show_photo_wk").fadeOut(200);
                        $(".back_shadow").fadeOut(400);
                        $(".show_avl").fadeOut(400);
                        self.state = false;
                    }
                }
            });
            $(".wrapper").on("click", function () {
                if (self.state == true) {
                    if ($scope.edit == true) {
                        $scope.myCalendar.fullCalendar('removeEvents');
                        $scope.origin_array.forEach(function (obj) {
                            $scope.myCalendar.fullCalendar("addEventSource", [obj]);
                        });
                    }
                    $(".up_data_wkp").fadeOut(200);
                    $(".show_photo_wk").fadeOut(200);
                    $(".back_shadow").fadeOut(400);
                    $(".show_avl").fadeOut(400);
                    self.state = false;
                }
            });
        },
        modal_img: function () {
            var alto = $(window).height();
            var ancho = $(window).width();
            $("#img_photo").css({"max-height": alto - $("#mod_desc").height() - 250 + "px", "max-width": ancho - 100 + "px"});
            $(window).resize(function () {
                var alto = $(window).height();
                var ancho = $(window).width();
                $("#img_photo").css({"max-height": alto - $("#mod_desc").height() - 250 + "px", "max-width": ancho - 100 + "px"})
            })
        },
        stop_prog: function () {
            $(".up_data_wkp").on("click", function (e) {
                e.stopPropagation();
            });
            $(".show_photo_wk").on("click", function (e) {
                e.stopPropagation();
            });
            $(".show_avl").on("click", function (e) {
                e.stopPropagation();
            });
        },
        textarea_resize: function () {
            $('.am_textarea').autosize();
        }
    };
    $scope.profile._constructor();
    $scope.profile._reviews();
    $scope.profile.back_shadow();
    $scope.profile.show_new_photo('#in_photo', '#pro_photo');
    $scope.profile.show_new_photo('#mid_pho', '#modal_up');
    $scope.profile.wrap_close();
    $scope.profile.modal_img();
    $scope.profile.stop_prog();
    $scope.profile.textarea_resize();


    var cssefects = {
        get_it: function () {
            $("#entedido").click("on", function () {
                $(".bloqueverde").slideUp(200);
            })
        }
    };
    cssefects.get_it();
});
