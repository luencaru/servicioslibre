/**
 * Created by carlos on 11/07/14.
 */
App.controller("subcategoryCtrl", function subcategoryCtrl($scope, $state, api, promise, $stateParams) {

    $scope.sub_ctrl = {
        data: null,
        _constructor: function () {
            var self = this;
            self.data = promise;
        },
        modal: function (item) {
            $scope.$emit("SEND_DATA", item);
        }
    };
    $scope.sub_ctrl._constructor();

    $scope.title_cat = $stateParams.name;

});