angular.module("suricats").controller("initializeCtrl", function ($scope, $state, api) {

    $scope.verified = verified;

    $scope.Routes = {
        profile: "/perfil",
        home: "/servicios",
        requests: "/solicitudes"
    };

    $scope.$on("SEND_DATA", function (e, data) {
        $scope.modal_data = data;
        $scope.ac_init.show_modal();
    });

    $scope.state_price=true;

    $scope.ac_init = {
        state: false,
        avail_state: false,
        show_modal: function () {
            var self = this;
            $(".back_shadow").fadeIn(200);
            $(".mod_aply").fadeIn(400);
            setTimeout(function () {
                self.state = true;
            }, 10);
        },
        close_modal: function () {
            $(".mod_aply").fadeOut(200);
            $(".aply_click").fadeOut(200);
            $(".back_shadow").fadeOut(400);
            $(".show_avl").fadeOut(400);
            this.state = false;
        },
        aplicar: function (aply_serv, category) {
            var categ = category[0];
            var self = this;
            if(aply_serv.price.$modelValue==undefined || aply_serv.price.$modelValue==""){
                $scope.state_price=false;
            }else{
                $scope.state_price=true;
                api.applies.customPOST({
                    service: categ[0],
                    proposed_price: aply_serv.price.$modelValue,
                    description: aply_serv.describe.$modelValue
                }).then(function () {
                    $(".mod_aply").fadeOut(200);
                    $(".aply_click").fadeIn(600);
                    setTimeout(function() {
                        $scope.modal_data.my_apply = 1;
                        $scope.price = "";
                        $scope.describe = "";
                    },200);
                });
            }
        },
        go_request: function (id) {
            $state.go("chat", {id: id});
            $(".mod_aply").fadeOut(200);
            $(".aply_click").fadeOut(200);
            $(".back_shadow").fadeOut(400);
            this.state = false;
        },
        back_shadow: function () {
            var height = $(window).height();
            $(".back_shadow").css("height", height + "px");
            $(window).resize(function () {
                var height = $(window).height();
                $(".back_shadow").css("height", height + "px");
            })
        },
        wrap_close: function () {
            var self = this;
            $(document).keydown(function (e) {
                if (e.keyCode === 27) {
                    if (self.state == true) {
                        $(".mod_aply").fadeOut(200);
                        $(".aply_click").fadeOut(200);
                        $(".back_shadow").fadeOut(400);
                        self.state = false;
                    }
                }
            });
            $(".wrapper").on("click", function () {
                if (self.state == true) {
                    $(".mod_aply").fadeOut(200);
                    $(".aply_click").fadeOut(200);
                    $(".back_shadow").fadeOut(400);
                    self.state = false;
                }
            })

        },
        stop_prog: function () {
            $(".mod_aply").on("click", function (e) {
                e.stopPropagation();
            });
            $(".aply_click").on("click", function (e) {
                e.stopPropagation();
            });
        }
    };

    $scope.ac_init.back_shadow();
    $scope.ac_init.wrap_close();
    $scope.ac_init.stop_prog();
});