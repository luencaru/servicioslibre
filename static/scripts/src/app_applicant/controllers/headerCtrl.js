function headerCtrl($scope,api,$state) {

    $scope.applicant = true;

    $scope.header = {
        data:null,
        _constructor: function() {
            var self = this;
            api.me.one("notifications").get().then(function(res){
                self.data = res.results;
                console.log(res.results,"notifications");
            });
        },
        go_service: function(id) {
            api.notifications.one(id).one("read").customPOST({
            }).then(function () {
                console.log("leido")
            });
            $state.go("chat", {id: id});
        }
    };

    $scope.header._constructor();

    var cssefect = {
        dropdown: function () {
            $(".du_dropdown").on("click", function () {
                $('.dropdown1').stop().slideToggle(200);
                $('.dropdown2').stop().slideUp(200);
                return false;
            });
            $(".du_notif").on("click", function(){
                $('.dropdown1').stop().slideUp(200);
                $('.dropdown2').stop().slideToggle(200);
                return false;
            });
            $('.wrapper').on("click", function () {
                $('.dropdown1').slideUp(200);
                $('.dropdown2').slideUp(200);
            });
        }
    };

    cssefect.dropdown();

    $(".dd_text").on("click", function (e) {
        e.stopPropagation();
    });
}