var App = angular.module('suricats', ['ngAnimate', 'ngMockE2E' , 'ngTagsInput', 'ngRoute' , 'ui.calendar' , 'restangular', 'ui.autocomplete', 'ngAutocomplete', 'ui.router', 'infinite-scroll', 'ngResource', 'angularFileUpload', 'ngCookies']).provider('$cookie', function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    this.$get = function () {
        return {
            csrftoken: getCookie('csrftoken'),
            rol: getCookie('rol')
        }
    }
}).config(['$routeProvider', '$urlRouterProvider', '$httpProvider', '$stateProvider', '$interpolateProvider', '$cookieProvider', '$locationProvider', '$provide', 'RestangularProvider', function ($routeProvider, $urlRouterProvider, $httpProvider, $stateProvider, $interpolateProvider, $cookieProvider, $locationProvider, $provide, RestangularProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
     $locationProvider.html5Mode(true);
    var cookie = $cookieProvider.$get();

    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') || !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                xhr.setRequestHeader("X-CSRFToken", cookie.csrftoken);
            }
        }
    });

    RestangularProvider.setResponseExtractor(function (response) {
        var newResponse = response;
        if (angular.isArray(response)) {
            angular.forEach(newResponse, function (value, key) {
                newResponse[key].originalElement = angular.copy(value);
            });
        } else {
            newResponse.originalElement = angular.copy(response);
        }
        return newResponse;
    });
    $httpProvider.defaults.headers.common['X-CSRFToken'] = cookie.csrftoken;

    $provide.decorator('$httpBackend', function ($delegate) {
        var proxy = function (method, url, data, callback, headers) {
            var interceptor = function () {
                var _this = this,

                    _arguments = arguments;
                setTimeout(function () {
                    callback.apply(_this, _arguments);
                }, 800);
            };
            return $delegate.call(this, method, url, data, interceptor, headers);
        };
        for (var key in $delegate) {
            proxy[key] = $delegate[key];
        }
        return proxy;
    });

    var _origin = window.location.origin + '/';
    var root = {
        name: 'root',
        url: '',
        abstract: true
    };

    var first_step = {
        name: "first_step",
        url: '/bienvenido',
        parent: "root",
        resolve: {
            promise: function ($state, api) {
                return api.me.one("services-home").get().then(function (res) {
                    if (res.is_new == false) {
                        $state.transitionTo('services');
                    }
                }, function (err) {
                    console.log(err);
                })
            }
        },
        views: {
            "header@": {
                templateUrl: _origin + "partials/header_first_step.html"
            },
            "@": {
                templateUrl: _origin + "partials/first_step.html",
                controller: "first_stepCtrl"
            },
            "footer@": {
                templateUrl: _origin + "partials/footer.html"
            }
        }
    };

    var account = {
        name: "account",
        url: '',
        parent: "root",
        abstract: true,
        resolve: {
            promise: function ($state, api) {
                return api.me.one("services-home").get().then(function (res) {
                    if (res.is_new == true) {
                        $state.transitionTo('first_step');
                    }
                }, function (err) {
                    console.log(err);
                })
            }
        },
        views: {
            "header@": {
                templateUrl: _origin + "partials/header_applicant.html",
                controller: "headerCtrl"
            },
            "footer@": {
                templateUrl: _origin + "partials/footer.html"
            }
        }
    };

    var services = {
        name: "services",
        url: '/servicios',
        parent: "account",
        views: {
            "@": {
                templateUrl: _origin + "partials/home.html",
                controller: "homeCtrl"
            }
        }
    };

    var profile = {
        name: "profile",
        url: '/perfil',
        parent: "account",
        views: {
            "@": {
                templateUrl: _origin + "partials/perfil.html",
                controller: "profileCtrl"
            }
        }
    };

    var requests = {
        name: "requests",
        url: '/solicitudes',
        parent: "account",
        views: {
            "@": {
                templateUrl: _origin + "partials/solicitudes.html",
                controller: "requestCtrl"
            }
        }
    };

    var category_g = {
        name: "category_g",
        url: '/recientes',
        parent: "services",
        resolve: {
            promise: function ($stateParams, api) {
                return api.me.one("services").get()
            }
        },
        views: {
            "@": {
                templateUrl: _origin + "partials/subcategory.html",
                controller: "subcategoryCtrl"
            }
        }
    };

    var subcategory = {
        name: "subcategory",
        url: '/:name/',
        parent: "services",
        resolve: {
            promise: function ($stateParams, api) {
                return api.me.one("services").get({'subc': $stateParams.name})
            }
        },
        views: {
            "@": {
                templateUrl: _origin + "partials/subcategory.html",
                controller: "subcategoryCtrl"
            }
        }
    };

    var chat = {
        name: "chat",
        url: '/:id/',
        parent: "requests",
        resolve: {
            promise: function ($stateParams) {
                return $stateParams.id;
            }
        },
        views: {
            "@": {
                templateUrl: _origin + "partials/conectado.html",
                controller: "chatCtrl"
            }
        }
    };

    $stateProvider.state(root).state(account).state(first_step).state(services).state(profile).state(subcategory).state(category_g).state(requests).state(chat);
    $urlRouterProvider.otherwise('/servicios');

}]).run(['$rootScope', '$state', '$stateParams', 'Restangular', '$httpBackend', function ($rootScope, $state, $stateParams, Restangular, $httpBackend) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    Restangular.setBaseUrl('/api');
    Restangular.setRequestSuffix('/');
    $httpBackend.whenGET(new RegExp('partials/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/me/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/me/.*')).passThrough();
    $httpBackend.whenPUT(new RegExp('/api/me/first-step')).passThrough();
    $httpBackend.whenPUT(new RegExp('/api/me/availability')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/me/profile-photo')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/services/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/applies/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/applies/.*')).passThrough();
    $httpBackend.whenPUT(new RegExp('/api/applies/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/categories/.*')).passThrough();
    $httpBackend.whenGET(new RegExp('/api/districts/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/messages/.*')).passThrough();
    $httpBackend.whenPOST(new RegExp('/api/notifications/.*')).passThrough();


    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);


App.factory('api', function (Restangular) {
    return {
        me: Restangular.one('me'),
        f_step: Restangular.one('me/first-step'),
        availability: Restangular.one('me/availability'),
        services: Restangular.one('services'),
        categories: Restangular.one('categories'),
        applies: Restangular.one('applies'),
        messages: Restangular.one('messages'),
        districts: Restangular.one('districts'),
        notifications: Restangular.one('notifications')
    }
});

App.directive('center', function () {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            function center() {
                element.css({
                    "position": "fixed",
                    "z-index": "111",
                    "top": $(window).height() / 2 - element.height() / 2,
                    "left": $(window).width() / 2 - element.width() / 2
                });
            }

            center();
            scope.$watch(function () {
                return element.height();
            }, function () {
                scope.$evalAsync(function () {
                    center();
                });
            });
            $(window).resize(function () {
                center();
            });
            scope.$on("art_center", function (e) {
                center();
            })
        }
    }
});

App.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                });
            });
        }
    }
}]);

App.directive('widthColor', function () {
    return {
        restric: 'A',
        scope: {
            totalstars: '=',
            ratedservices: '='
        },
        link: function (scope, element) {
            function colorbarra() {
                var ancho = scope.totalstars / scope.ratedservices;
                var color = 23 * ancho;
                element.css('width', color + 'px');
            }

            scope.$watch("totalstars", function () {
                colorbarra()
            })
        }
    }
});

App.directive('barraStar', function () {
    return {
        restrict: 'A',
        scope: {
            datastar1: '=',
            datastar2: '=',
            datastar3: '=',
            datastar4: '=',
            datastar5: '=',
            datastarstate: '='
        },
        link: function (scope, element) {
            function colorbarra() {
                var max = 0;
                var array = [scope.datastar1, scope.datastar2, scope.datastar3, scope.datastar4, scope.datastar5]
                for (var i = 1; i <= array.length; i++) {
                    if (array[i] > max) {
                        max = array[i];
                    }
                }
                var color_bar = (103 * scope.datastarstate) / max;
                element.css('width', color_bar + 'px')
            }

            scope.$watch("datastar1", function () {
                colorbarra()
            })
        }
    }
});

App.directive('artSlider', function ($timeout) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            element.addClass("art-slider")
            var array = [];
            scope.$watch(attrs.artdata, function (sew, old) {
                if (sew) {
                    array = sew;
                }
            });
            var prev = element.find(".prev");
            var next = element.find(".next");
            next.bind("click", function () {
                var first = array[0];
                first.first = "af-true";
                scope.$apply();
                $timeout(function () {
                    var top = array.shift();
                    array.push(top);
                    $timeout(function () {
                        first.first = null;
                    })
                }, 500)
            });
            prev.bind("click", function () {
                var last = array[0];
                last.first = "al-true";
                scope.$apply();
                $timeout(function () {
                    var top = array.pop();
                    array.unshift(top);
                    $timeout(function () {
                        last.first = null;
                    })
                }, 500)
            });
        }
    }
});

App.directive('imgFacebook', function () {
    return {
        restrict: "A",
        scope: {
            src: '='
        },
        link: function (scope, element, attrs) {
            var image = element;

            function size_image(img) {
                if (img.width() > img.height()) {
                    element.css("height", "110px")
                } else {
                    element.css("width", "152.5px")
                }
            }

            scope.$watch("src", function () {
                size_image(image)
            })
        }
    }
});