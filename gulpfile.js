var gulp = require('gulp'),
    minifyCSS = require('gulp-minify-css'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglifyjs');

gulp.task('minify-css', function() {
    gulp.src(
        [
            'static/styles/style.css',
            'static/styles/autocomplete.css',
            'static/styles/font-awesome.css',
            'static/styles/fullcalendar.css',
            'static/styles/fullcalendar.print.css',
            'static/styles/main.min.css'
        ])
        .pipe(minifyCSS({keepBreaks:false}))
        .pipe(gulp.dest('./static/styles/'))
});

gulp.task('concat', function () {
    gulp.src(
        [
            'static/styles/style.css',
            'static/styles/autocomplete.css',
            'static/styles/font-awesome.css',
            'static/styles/fullcalendar.css',
            'static/styles/fullcalendar.print.css'
        ])
        .pipe(concatCss("main.min.css"))
        .pipe(gulp.dest('./static/styles/'));
});

gulp.task('script', function () {
    gulp.src(
        [
            './static/scripts/vendor/jquery.min.js',
            './static/scripts/vendor/jquery.autosize.js',
            './static/scripts/vendor/textarea-expander.js',
            './static/scripts/vendor/angular.min.js',
            './static/scripts/vendor/app_calendar/jquery-ui/ui/jquery.ui.core.js',
            './static/scripts/vendor/app_calendar/jquery-ui/ui/jquery.ui.widget.js',
            './static/scripts/vendor/app_calendar/jquery-ui/ui/jquery.ui.mouse.js',
            './static/scripts/vendor/app_calendar/jquery-ui/ui/jquery.ui.draggable.js',
            './static/scripts/vendor/app_calendar/jquery-ui/ui/jquery.ui.droppable.js',
            './static/scripts/vendor/app_calendar/jquery-ui/ui/jquery.ui.resizable.js',
            './static/scripts/vendor/fullcalendar.js',
            './static/scripts/vendor/calendar.js',
            './static/scripts/vendor/angular-route.js',
            './static/scripts/vendor/ng-animate.js',
            './static/scripts/vendor/angular-mocks.js',
            './static/scripts/vendor/ng-tags-input.js',
            './static/scripts/vendor/lodash.underscore.min.js',
            './static/scripts/vendor/restangular.js',
            './static/scripts/vendor/autocomplete.js',
            './static/scripts/vendor/ngAutocomplete.js',
            './static/scripts/vendor/angular-ui-router.js',
            './static/scripts/vendor/infinite-scroll.js',
            './static/scripts/vendor/angular-resource.min.js',
            './static/scripts/vendor/fileUp.js',
            './static/scripts/vendor/angular-cookies.js',
            './static/scripts/src/app_applicant/suricats.js',
            './static/scripts/src/app_applicant/controllers/*.js',
            './static/scripts/src/app/suricats.js',
            './static/scripts/src/app/controllers/*.js',
            './static/scripts/src/registro.js'
        ])
        .pipe(uglify('all.min.js'))
        .pipe(gulp.dest('./static/scripts/dist/'))
});

gulp.task('default', ['minify-css']);
