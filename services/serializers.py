from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Service, Apply, District
from .fields import TotalStarsByApply, DoneTasksByApply, RatedServicesByApply, MyApplyField


class CreateApplySerializer(serializers.ModelSerializer):
    proposed_price = serializers.FloatField(required=True)

    def validate_proposed_price(self, attrs, source):
        value = attrs[source]
        if value == 0:
            raise serializers.ValidationError("Proposed price must be greater than 0")
        return attrs

    class Meta:
        model = Apply
        fields = ("id", "service", "proposed_price", "description")


class MiniServiceSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%b %d, %Y. %I:%m %p')
    date = serializers.DateField(source='deadline_date', format='%b %d, %Y.')
    hour = serializers.TimeField(source='deadline_hour', format='%I:%m %p')
    subcategory = serializers.SlugField(source='subcategory.slug')
    image = serializers.CharField(source='subcategory.photo_url')
    photo = serializers.CharField(source='owner.profile.photo_url')
    category = serializers.SlugField(source='subcategory.category.slug')
    my_apply = MyApplyField(source='*')

    class Meta:
        model = Service
        fields = ("id", "photo", "address", "created_at", "subcategory", "category", "image", "details", "date", "hour",
                  "my_apply")


class MiniMyServiceSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%b %d, %Y. %I:%m %p')
    date = serializers.DateField(source='deadline_date', format='%b %d, %Y.')
    hour = serializers.TimeField(source='deadline_hour', format='%I:%m %p')
    subcategory = serializers.CharField(source='subcategory.name')
    subcategory_slug = serializers.SlugField(source='subcategory.slug')
    image = serializers.CharField(source='subcategory.photo_url')
    photo = serializers.CharField(source='owner.profile.photo_url')
    category = serializers.SlugField(source='subcategory.category.slug')
    number_proposals = serializers.IntegerField(source='number_proposals')

    class Meta:
        model = Service
        fields = ("id", "photo", "address", "created_at", "subcategory", "subcategory_slug", "category", "image",
                  "details", "date", "hour", "number_proposals", "done_at")


class MyApplySerializer(serializers.ModelSerializer):
    photo = serializers.CharField(source='photo')
    fullname = serializers.CharField(source='owner.get_full_name')
    address = serializers.CharField(source='owner.profile.address')
    image = serializers.CharField(source='service.subcategory.photo_url')
    subcategory = serializers.CharField(source='service.subcategory')
    date = serializers.DateField(source='service.deadline_date', format='%b %d, %Y.')
    hour = serializers.TimeField(source='service.deadline_hour', format='%I:%m %p')
    details = serializers.Field(source='service.details')

    class Meta:
        model = Apply
        fields = ("id", "date", "hour", "photo", "image", "address", "fullname", "address", "price",
                  "proposed_price", "client_proposed_price", "last_propose_applicant", "subcategory", "details")


class ApplySerializer(serializers.ModelSerializer):
    photo = serializers.CharField(source='photo')
    fullname = serializers.CharField(source='owner.get_full_name')
    address = serializers.CharField(source='owner.profile.address')
    total_stars = TotalStarsByApply(source='*')
    done_services = DoneTasksByApply(source='*')
    rated_services = RatedServicesByApply(source='*')

    class Meta:
        model = Apply
        fields = ("id", "photo", "fullname", "address", "total_stars", "rated_services", "done_services",
                  "price", "proposed_price", "client_proposed_price")


class ServiceSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%b %d, %Y. %I:%m %p')
    date = serializers.DateField(source='deadline_date', format='%b %d, %Y.')
    hour = serializers.TimeField(source='deadline_hour', format='%I:%m %p')
    subcategory = serializers.CharField(source='subcategory.name')
    subcategory_slug = serializers.SlugField(source='subcategory.slug')
    image = serializers.CharField(source='subcategory.photo_url')
    photo = serializers.CharField(source='owner.profile.photo_url')
    category = serializers.CharField(source='subcategory.category.name')
    category_slug = serializers.SlugField(source='subcategory.category.slug')
    applies = ApplySerializer(many=True)
    district = serializers.CharField(source='district.name')

    class Meta:
        model = Service
        fields = ("id", "photo", "address", "created_at", "subcategory", "subcategory_slug", "category",
                  "category_slug", "image", "details", "date", "hour", "applies")


class DetailApplySerializer(serializers.ModelSerializer):
    photo = serializers.CharField(source='photo')
    date = serializers.DateField(source='service.deadline_date', format='%b %d, %Y.')
    hour = serializers.TimeField(source='service.deadline_hour', format='%I:%m %p')
    owner = serializers.IntegerField(source='owner.id')
    fullname = serializers.CharField(source='owner.get_full_name')
    subcategory = serializers.CharField(source='service.subcategory')
    address = serializers.CharField(source='owner.profile.address')
    total_stars = TotalStarsByApply(source='*')
    done_services = DoneTasksByApply(source='*')
    rated_services = RatedServicesByApply(source='*')

    class Meta:
        model = Apply
        fields = ("id", "photo", "date", "subcategory", "first_view", "price", "fullname", "description",
                  "total_stars", "rated_services", "done_services", "proposed_price", "hour", "client_proposed_price",
                  "address", "last_propose_applicant")


class CreateServiceSerializer(serializers.ModelSerializer):
    district = serializers.PrimaryKeyRelatedField(queryset=District.objects.filter(active=True))

    class Meta:
        model = Service
        fields = ("details", "subcategory", "address", "deadline_date", "deadline_hour", "district")


class DoneServiceSerializer(serializers.ModelSerializer):
    datetime = serializers.DateTimeField(source='created_at', format='%b %d, %Y., %I:%m %p')
    fullname = serializers.CharField(source='task.user.get_full_name')
    address = serializers.CharField(source='task.address')
    stars = serializers.CharField(source='score')

    class Meta:
        model = Service
        fields = ("id", "datetime", "fullname", "address", "stars", "review")


class ServicesHomeSerializer(serializers.ModelSerializer):
    last_services = MiniServiceSerializer(many=True, source='profile.last_services')
    other_services = serializers.SerializerMethodField('get_services_for_user')
    is_new = serializers.BooleanField(source='profile.is_new')

    def get_services_for_user(self, obj):
        r = []
        subcategories = obj.profile.subcategories.all()
        if len(subcategories) > 1:
            for s in subcategories:
                ss = Service.objects.filter(done_by__isnull=True, subcategory=s).order_by('-created_at')[:5]
                r.append({'name': s.name,
                          'data': MiniServiceSerializer(ss, many=True, context=self.context).data,
                          "cover": s.cover_url,
                          "color": s.color})
        return r

    class Meta:
        model = User
        fields = ("is_new", "last_services", "other_services")


class CreateReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ("done_score", "done_review")


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ("done_at", "done_by", "done_score", "done_review")