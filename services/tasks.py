from celery import shared_task
from account.models import TimeAvailability
from .models import Apply


@shared_task
def search_applicants(service, *args, **kwargs):
    weekday = service.deadline_date.weekday()
    weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    hour = service.deadline_hour
    lookups = {weekdays[weekday] + '_since__lte': hour, weekdays[weekday] + '_to__gte': hour}
    users_id = TimeAvailability.objects.values_list('owner', flat=True).filter(**lookups)
    profiles = service.district.applicants_work.filter(user__in=users_id, verified=True)
    Apply.objects.bulk_create([Apply(owner=p.user, service=service) for p in profiles])