from django.conf.urls import patterns, url
from django.views.generic.base import TemplateView
from services.views import CreateReviewAPI, MarkAsReadApplyAPI, ListMyReviewsAPI, RequestServiceByView
from .views import (ListServicesAPI, DetailUpdateApplyAPI, HomeView, RequestServiceView, CreateServiceAPI,
                    IndexView, DetailHomeApplicantAPI, ApplyToServiceAPI, ListMyAppliesAPI, ListSubcategoriesAPI,
                    ListCategoriesAPI, ProfileAPI, ListReviewsAPI, RetrieveServiceAPI, ListDistrictsAPI)

urlpatterns = patterns(
    '',
    url(r'api/me/services-home/', DetailHomeApplicantAPI.as_view(), name='services-for-me'),
    url(r'^api/me/services/$', ListServicesAPI.as_view(), name="my-created-services"),
    url(r'^api/me/reviews/$', ListMyReviewsAPI.as_view(), name="my-reviews"),
    url(r'^api/me/applies/$', ListMyAppliesAPI.as_view(), name="my-applies"),
    url(r'^api/services/$', CreateServiceAPI.as_view(), name="create-service"),
    url(r'^api/services/(?P<pk>\d+)/review/$', CreateReviewAPI.as_view(), name="create-review"),
    url(r'^api/categories/$', ListCategoriesAPI.as_view(), name="list-categories"),
    url(r'^api/categories/(?P<pk>[-\w]+)/subcategories/$', ListSubcategoriesAPI.as_view(), name="list-subcategories"),
    url(r'^api/districts/$', ListDistrictsAPI.as_view(), name="list-districts"),
    url(r'^api/services/(?P<pk>\d+)/$', RetrieveServiceAPI.as_view(), name="retrieve-service"),
    url(r'^api/applies/$', ApplyToServiceAPI.as_view(), name="apply-service"),
    url(r'^api/applies/(?P<pk>\d+)/$', DetailUpdateApplyAPI.as_view(), name="detail-apply"),
    url(r'^api/applies/(?P<pk>\d+)/read/$', MarkAsReadApplyAPI.as_view(), name="mark-as-read-apply"),
    url(r'^api/applies/(?P<pk>\d+)/user/$', ProfileAPI.as_view(), name="detail-user-proposal"),
    url(r'^api/applies/(?P<pk>\d+)/user/reviews/$', ListReviewsAPI.as_view(), name="detail-user-proposal"),

    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^servicios', IndexView.as_view(), name='index-servicios'),
    url(r'^perfil$', TemplateView.as_view(template_name='index_applicant.html'), name='index-perfil'),
    url(r'^solicitudes', TemplateView.as_view(template_name='index_applicant.html'), name='index-solicitudes'),
    url(r'^propuesta', IndexView.as_view(), name='index-solicitudes'),
    url(r'^bienvenido$', IndexView.as_view(), name='index-solicitudes'),
    url(r'^inicio/$', HomeView.as_view(), name="home"),
    url(r'^solicitar$', TemplateView.as_view(template_name='index.html'), name='index-solicitar'),
    url(r'^solicitar-servicio/$', RequestServiceView.as_view(), name="request-service"),
    url(r'^solicitar-servicio/(?P<slug>[-\w]+)/$', RequestServiceByView.as_view(), name="request-service-by"),
)