from django.contrib.auth.models import User
from django.db import models
from django_hstore import hstore


class District(models.Model):
    postal_code = models.CharField(max_length=7, primary_key=True)
    name = models.CharField(max_length=40)
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class Category(models.Model):
    slug = models.SlugField(max_length=30, primary_key=True)
    name = models.CharField(max_length=30)
    image = models.ImageField(upload_to='categories', null=True, blank=True)

    def __unicode__(self):
        return self.name


class SubCategory(models.Model):
    slug = models.SlugField(max_length=30, primary_key=True)
    name = models.CharField(max_length=30)
    category = models.ForeignKey('Category')
    photo = models.ImageField(upload_to='subcategories/', null=True, blank=True)
    cover = models.ImageField(upload_to='subcategories/cover', null=True, blank=True)
    color = models.CharField(max_length=25, null=True, blank=True)

    def photo_url(self):
        return self.photo.url if self.photo else None

    def cover_url(self):
        return self.cover.url if self.cover else None

    def __unicode__(self):
        return self.name


class Service(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    district = models.ForeignKey('District', null=True)
    address = models.CharField(max_length=100)
    owner = models.ForeignKey(User, related_name='created_services')
    subcategory = models.ForeignKey('SubCategory')
    details = hstore.DictionaryField(null=True, blank=True)
    done_at = models.DateTimeField(null=True, blank=True)
    done_by = models.ForeignKey(User, related_name='done_services', null=True, blank=True)
    done_score = models.IntegerField(null=True, blank=True)
    done_review = models.TextField(blank=True)
    deadline_date = models.DateField(null=True, blank=True)
    deadline_hour = models.TimeField(null=True, blank=True)
    asap = models.NullBooleanField(default=None, null=True, blank=True)
    closed = models.BooleanField(default=False)

    objects = hstore.HStoreManager()

    def __unicode__(self):
        return u'{} | {} | {}'.format(self.deadline_date, self.address, self.subcategory)

    @property
    def photo(self):
        return self.subcategory.photo.url if self.subcategory.photo else None

    @property
    def number_proposals(self):
        return self.applies.count()

    def name_done_by(self):
        return self.done_by.get_full_name() if self.done_by else None


class Apply(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, related_name='applies')
    service = models.ForeignKey('Service', related_name='applies')
    proposed_price = models.FloatField(null=True, blank=True)
    client_proposed_price = models.FloatField(null=True, blank=True)
    last_propose_applicant = models.NullBooleanField(default=None)
    price = models.FloatField(null=True, blank=True)
    description = models.TextField(blank=True)
    first_view = models.BooleanField(default=True)
    recommended = models.BooleanField(default=False)

    @property
    def photo(self):
        return self.owner.profile.photo.url if self.owner.profile.photo else None

    class Meta:
        unique_together = 'owner', 'service'
        ordering = "created_at", "service"

    def __unicode__(self):
        return u'{} already applied to {}'.format(self.owner.email, self.service)