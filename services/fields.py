from django.db.models import Sum
from rest_framework import fields
from .models import Apply


class MyApplyField(fields.Field):
    def to_native(self, service):
        request = self.context.get('request', None)
        if request and not request.user.is_superuser:
            apply = Apply.objects.filter(service=service, owner=request.user).first()
            if apply:
                return apply.id
        return None


class RatedServicesByApply(fields.Field):
    def to_native(self, apply):
        request = self.context.get('request', None)
        if request and not request.user.is_superuser:
            return apply.owner.done_services.filter(subcategory=apply.service.subcategory, done_score__gt=0).count()
        return 0


class TotalStarsByApply(fields.Field):
    def to_native(self, apply):
        request = self.context.get('request', None)
        if request and not request.user.is_superuser:
            val = apply.owner.done_services.filter(subcategory=apply.service.subcategory).aggregate(Sum('done_score'))
            return val['done_score__sum'] if val['done_score__sum'] else 0
        return 0


class DoneTasksByApply(fields.Field):
    def to_native(self, apply):
        request = self.context.get('request', None)
        if request and not request.user.is_superuser:
            return apply.owner.done_services.filter(subcategory=apply.service.subcategory).count()
        return 0