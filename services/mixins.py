from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import redirect


class NotUserOrApplicantMixin(object):
    authenticated_redirect_url = settings.LOGIN_REDIRECT_URL

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated() and hasattr(request.user, 'profile'):
            return redirect(self.get_authenticated_redirect_url())
        return super(NotUserOrApplicantMixin, self).dispatch(request, *args, **kwargs)

    def get_authenticated_redirect_url(self):
        """ Return the reversed authenticated redirect url. """
        if not self.authenticated_redirect_url:
            raise ImproperlyConfigured(
                '{0} is missing an authenticated_redirect_url '
                'url to redirect to. Define '
                '{0}.authenticated_redirect_url or override '
                '{0}.get_authenticated_redirect_url().'.format(
                    self.__class__.__name__))
        return self.authenticated_redirect_url