# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='apply',
            options={'ordering': ('created_at', 'service')},
        ),
        migrations.RemoveField(
            model_name='apply',
            name='connected',
        ),
        migrations.AddField(
            model_name='apply',
            name='recommended',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
