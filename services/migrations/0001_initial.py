# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django_hstore.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Apply',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('connected', models.BooleanField(default=False)),
                ('proposed_price', models.FloatField(null=True, blank=True)),
                ('client_proposed_price', models.FloatField(null=True, blank=True)),
                ('last_propose_applicant', models.NullBooleanField(default=None)),
                ('price', models.FloatField(null=True, blank=True)),
                ('description', models.TextField(blank=True)),
                ('first_view', models.BooleanField(default=True)),
                ('owner', models.ForeignKey(related_name=b'applies', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('connected', 'created_at'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('slug', models.SlugField(max_length=30, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('image', models.ImageField(null=True, upload_to=b'categories', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('postal_code', models.CharField(max_length=7, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('address', models.CharField(max_length=100)),
                ('details', django_hstore.fields.DictionaryField(null=True, blank=True)),
                ('done_at', models.DateTimeField(null=True, blank=True)),
                ('done_score', models.IntegerField(null=True, blank=True)),
                ('done_review', models.TextField(blank=True)),
                ('deadline_date', models.DateField(null=True, blank=True)),
                ('deadline_hour', models.TimeField(null=True, blank=True)),
                ('asap', models.NullBooleanField(default=None)),
                ('closed', models.BooleanField(default=False)),
                ('district', models.ForeignKey(to='services.District', null=True)),
                ('done_by', models.ForeignKey(related_name=b'done_services', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('owner', models.ForeignKey(related_name=b'created_services', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('slug', models.SlugField(max_length=30, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('photo', models.ImageField(null=True, upload_to=b'subcategories/', blank=True)),
                ('cover', models.ImageField(null=True, upload_to=b'subcategories/cover', blank=True)),
                ('color', models.CharField(max_length=25, null=True, blank=True)),
                ('category', models.ForeignKey(to='services.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='service',
            name='subcategory',
            field=models.ForeignKey(to='services.SubCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='apply',
            name='service',
            field=models.ForeignKey(related_name=b'applies', to='services.Service'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='apply',
            unique_together=set([('owner', 'service')]),
        ),
    ]
