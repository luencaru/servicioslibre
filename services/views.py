from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.db.models.aggregates import Count
from django.shortcuts import render, get_object_or_404
from django.utils.timezone import now
from django.views.generic import TemplateView, RedirectView
from django.views.generic.list import ListView
from rest_framework import generics, status, mixins
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from account.serializers import ProfileSerializer
from .mixins import NotUserOrApplicantMixin
from .models import Service, Apply, SubCategory, Category, District
from notifications.models import Notification
from .serializers import (DetailApplySerializer, CreateServiceSerializer, ServicesHomeSerializer, CreateApplySerializer,
                          MyApplySerializer, CreateReviewSerializer, ReviewSerializer, MiniMyServiceSerializer,
                          ServiceSerializer, MiniServiceSerializer)
from account.permissions import IsVerified
from .tasks import search_applicants


class DetailHomeApplicantAPI(generics.RetrieveAPIView):
    model = User
    serializer_class = ServicesHomeSerializer

    def get_object(self, queryset=None):
        return self.request.user


class HomeView(NotUserOrApplicantMixin, TemplateView):
    template_name = 'home.html'


class IndexView(RedirectView):
    url = reverse_lazy('services:home')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated() and hasattr(request.user, 'profile'):
            if request.user.profile.is_applicant:
                return render(request, 'index_applicant.html')
            return render(request, 'index.html')
        return super(IndexView, self).dispatch(request, *args, **kwargs)


class RequestServiceView(NotUserOrApplicantMixin, TemplateView):
    template_name = 'request.html'


class RequestServiceByView(NotUserOrApplicantMixin, ListView):
    template_name = 'prueba.html'
    model = SubCategory

    def get_queryset(self):
        return self.model.objects.filter(category__slug=self.kwargs.get('slug'))


class ListServicesAPI(generics.ListAPIView):
    model = Service
    serializer_class = MiniServiceSerializer
    paginate_by = 10
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if not self.request.user.profile.is_applicant:
            return MiniMyServiceSerializer
        return super(ListServicesAPI, self).get_serializer_class()

    def get_queryset(self):
        done = self.request.GET.get('done')
        if not self.request.user.profile.is_applicant:
            queryset = self.request.user.created_services.all()
        else:
            subcs_id = self.request.user.profile.subcategories.values_list("slug", flat=True)
            queryset = self.model.objects.filter(subcategory__in=subcs_id)
        if done == "1":
            queryset = queryset.filter(done_by__isnull=False)
        elif done == "0":
            queryset = queryset.filter(done_by__isnull=False)
        subcategory = self.request.GET.get('subc')
        if subcategory:
            queryset = queryset.filter(subcategory__slug=subcategory)
        applies = self.request.GET.get('applies')
        if applies == "1":
            queryset = queryset.annotate(applies_count=Count('applies')).filter(applies_count__gt=0)
        elif applies == "0":
            queryset = queryset.annotate(applies_count=Count('applies')).filter(applies_count=0)
        # codigo antiguo para ordenar segun score de usuario
        # l = list(self.model.objects.filter(task__id=solicitud))
        # return sorted(l, key=lambda o: -o.user.profile.calculate_stars_by_subc(o.task.subcategory))
        return queryset.order_by('-created_at')


class RetrieveServiceAPI(generics.RetrieveAPIView):
    model = Service
    serializer_class = ServiceSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)


class ListMyAppliesAPI(generics.ListAPIView):
    model = Apply
    serializer_class = MyApplySerializer
    paginate_by = 10
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = self.request.user.applies.all()
        subcategory = self.request.GET.get('subc')
        if subcategory:
            queryset = queryset.filter(service__subcategory__id=subcategory)
        return queryset


class DetailUpdateApplyAPI(generics.RetrieveAPIView):
    model = Apply
    serializer_class = DetailApplySerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.request.user.profile.is_applicant:
            return MyApplySerializer
        return super(DetailUpdateApplyAPI, self).get_serializer_class()

    def put(self, *args, **kwargs):
        new_price = self.request.DATA.get("new_price")
        accepted = self.request.DATA.get("accepted")
        self.object = self.get_object()
        if new_price:
            try:
                new_price = float(new_price)
            except ValueError:
                new_price = None
            if new_price:
                if self.request.user.profile.is_applicant:
                    self.object.proposed_price = new_price
                    self.object.last_propose_applicant = True
                else:
                    self.object.client_proposed_price = new_price
                    self.object.last_propose_applicant = False
                self.post_save(self.object)
                self.object.save()
                return Response({"detail": "New proposed price saved"}, status=status.HTTP_200_OK)
        elif accepted:
            if not self.object.price:
                if self.request.user.profile.is_applicant and not self.object.last_propose_applicant:
                    self.object.price = self.object.client_proposed_price
                    self.object.save()
                    self.post_save(self.object)
                    return Response({"detail": "Acepted last proposed price"}, status=status.HTTP_200_OK)
                elif not self.request.user.profile.is_applicant and self.object.last_propose_applicant:
                    self.object.price = self.object.proposed_price
                    self.object.save()
                    self.post_save(self.object)
                    return Response({"detail": "Acepted last proposed price"}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def post_save(self, obj, created=False):
        Notification.objects.create(
            owner=obj.service.owner if obj.last_propose_applicant else obj.owner,
            type=Notification.ACCEPTED_PROPOSAL if obj.price else Notification.COUNTER_PROPOSAL, object_id=obj.id)


class CreateServiceAPI(generics.CreateAPIView):
    model = Service
    serializer_class = CreateServiceSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsVerified)

    def pre_save(self, obj):
        obj.owner = self.request.user

    def post_save(self, obj, created=False):
        search_applicants.delay(obj)


class ApplyToServiceAPI(generics.CreateAPIView):
    model = Apply
    serializer_class = CreateApplySerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsVerified)

    def create(self, request, *args, **kwargs):
        instance = get_object_or_404(Service, id=request.DATA.get('service'))
        if instance.done_at:
            return Response({'detail': 'This service was already done.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        return super(ApplyToServiceAPI, self).create(request, *args, **kwargs)

    def pre_save(self, obj):
        obj.owner = self.request.user
        obj.last_propose_applicant = True

    def post_save(self, obj, created=False):
        Notification.objects.create(owner=obj.service.owner, type=Notification.NEW_APPLY, object_id=obj.id)


class MarkAsReadApplyAPI(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsVerified)

    def post(self, request, *args, **kwargs):
        apply_id = self.kwargs.get('pk')
        ap = Apply.objects.filter(id=apply_id).first()
        if ap and self.request.user == ap.service.owner:
            ap.first_view = False
            ap.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class CreateReviewAPI(mixins.UpdateModelMixin, generics.GenericAPIView):
    model = Service
    model_serializer_class = CreateReviewSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsVerified)

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def pre_save(self, obj):
        obj.done_by = self.request.user
        obj.done_at = now()


class ListMyReviewsAPI(generics.ListAPIView):
    model = Service
    model_serializer_class = ReviewSerializer
    paginate_by = 5
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.model.objects.filter(done_by=self.request.user)


class ProfileAPI(generics.RetrieveAPIView):
    model = User
    serializer_class = ProfileSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        ap = get_object_or_404(Apply, id=self.kwargs.get('pk'))
        self.subcategory = ap.service.subcategory
        return ap.owner


class ListReviewsAPI(generics.ListAPIView):
    model = Service
    model_serializer_class = ReviewSerializer
    paginate_by = 5
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        ap = get_object_or_404(Apply, id=self.kwargs.get('pk'))
        return self.model.objects.filter(done_by=ap.owner, subcategory=ap.service.subcategory)


class ListSubcategoriesAPI(generics.ListAPIView):
    model = SubCategory

    def get_queryset(self):
        return self.model.objects.filter(category__pk=self.kwargs.get('pk'))


class ListDistrictsAPI(generics.ListAPIView):
    model = District

    def get_queryset(self):
        if self.request.GET.get('available'):
            return self.model.objects.filter(active=True)
        return self.model.objects.all()


class ListCategoriesAPI(generics.ListAPIView):
    model = Category
