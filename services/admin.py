from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import Category, SubCategory, Apply, Service


class ServiceAdmin(ModelAdmin):
    list_display = 'id', 'deadline_date', 'deadline_hour', 'created_at', 'owner', 'subcategory'


class ApplyAdmin(ModelAdmin):
    list_display = 'id', 'created_at', 'owner', 'service', 'auto', 'description'

    def auto(self, obj):
        return True if not obj.client_proposed_price and not obj.proposed_price else False


class CategoryAdmin(ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, CategoryAdmin)
admin.site.register(Apply, ApplyAdmin)
admin.site.register(Service, ServiceAdmin)