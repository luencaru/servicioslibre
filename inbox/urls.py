from django.conf.urls import patterns, url
from .views import SendMessageAPI, ListMessagesAPI

urlpatterns = patterns(
    '',
    url(r'^api/messages/send/$', SendMessageAPI.as_view(), name="send-message"),
    url(r'^api/applies/(?P<pk>\d+)/messages/$', ListMessagesAPI.as_view(), name="list-messages"),
)
