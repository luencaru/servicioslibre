from rest_framework import serializers
from .models import Message


class SendMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("id", "apply", "content")


class ListMessageSerializer(serializers.ModelSerializer):
    fullname = serializers.CharField(source='owner.get_full_name')
    photo = serializers.CharField(source='owner.profile.photo_url')
    datetime = serializers.DateTimeField(source='created_at', format='%b %d, %Y., %I:%m %p')

    class Meta:
        model = Message
        fields = ("id", "fullname", "photo", "content", "datetime")