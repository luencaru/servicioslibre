# coding=utf-8
from django.contrib.auth.models import User
from django.db import models
from services.models import Apply


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    apply = models.ForeignKey(Apply, related_name='messages')
    owner = models.ForeignKey(User, related_name='sent_messages')
    content = models.TextField()

    def __unicode__(self):
        return u'{}: {}'.format(self.owner.email, self.content[:20])

    class Meta:
        ordering = ('-created_at',)