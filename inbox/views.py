from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from notifications.models import Notification

from .serializers import SendMessageSerializer, ListMessageSerializer
from .models import Message
from services.models import Apply
from account.permissions import IsVerified


class SendMessageAPI(generics.CreateAPIView):
    model = Message
    serializer_class = SendMessageSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsVerified)

    def pre_save(self, obj):
        obj.owner = self.request.user

    def post_save(self, obj, created=False):
        owner = obj.apply.owner if self.request.user != obj.apply.owner else obj.apply.service.owner
        Notification.objects.create(owner=owner, type=Notification.NEW_MESSAGE, object_id=obj.apply.id)


class ListMessagesAPI(generics.ListAPIView):
    model = Message
    serializer_class = ListMessageSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    paginate_by = 5

    def get_queryset(self):
        apply_id = self.kwargs.get('pk')
        apply = get_object_or_404(Apply, id=apply_id)
        return apply.messages.all()


class ListPossibleLinksAPI(generics.GenericAPIView):
    pass