from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework import mixins


class MCreateAPiVew(CreateAPIView):
    """
    CreateAPIView with multiple serializer_class, one for read and one for return.
    """
    response_serializer_class = None

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            response_serializer = self.get_response_serializer(self.object)
            headers = self.get_success_headers(serializer.data)
            return Response(response_serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_response_serializer(self, instance=None):
        """
        Return the serializer instance that will be used for the response
        """
        response_serializer_class = self.get_response_serializer_class()
        context = self.get_serializer_context()
        return response_serializer_class(instance, context=context)

    def get_response_serializer_class(self):
        """
        Return the class to use for the response serializer.
        Defaults to using `self.response_serializer_class`.
        """
        response_serializer_class = self.response_serializer_class
        if response_serializer_class is not None:
            return response_serializer_class

        serializer_class = self.serializer_class
        if serializer_class is not None:
            return serializer_class

        assert self.model is not None, \
            "'%s' should either include a 'serializer_class' attribute, " \
            "or use the 'model' attribute as a shortcut for " \
            "automatically generating a serializer class." \
            % self.__class__.__name__

        class DefaultSerializer(self.model_serializer_class):
            class Meta:
                model = self.model

        return DefaultSerializer


class MListCreateAPIView(mixins.ListModelMixin, MCreateAPiVew):
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)