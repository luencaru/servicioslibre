from django.conf import settings
from django.conf.urls import patterns, include, url, static
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    # url('^', include('social.apps.django_app.urls', namespace='social')),
    url(r'^', include('inbox.urls', namespace="inbox")),
    url(r'^', include('account.urls', namespace="account")),
    url(r'^', include('services.urls', namespace="services")),
    url(r'^', include('notifications.urls', namespace="notifications")),
    url(r'^partials/', include('partials.urls', namespace='partials')),
)

if settings.DEBUG:
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
