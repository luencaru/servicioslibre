from django.views.generic.list import ListView
from services.models import Category


class ServicesPartialView(ListView):
    template_name = "partials/servicios.html"
    model = Category
    context_object_name = 'categories'