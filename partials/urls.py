from django.conf.urls import patterns, url
from django.contrib import admin
from django.views.generic import TemplateView as T
from partials.views import ServicesPartialView

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^header_log.html$', T.as_view(template_name="partials/header_log.html"), name='partial-header_logeado'),
    url(r'^header_applicant.html$', T.as_view(template_name="partials/header_applicant.html")),
    url(r'^footer.html$', T.as_view(template_name="partials/footer.html"), name='partial-footer'),
    url(r'^conectado.html$', T.as_view(template_name="partials/conectado.html"), name='partial-conectado'),
    url(r'^perfil.html$', T.as_view(template_name="partials/perfil.html"),
        name='partial-perfil'),
    url(r'^solicitudes.html$', T.as_view(template_name="partials/solicitudes.html"),
        name='partial-solicitudes'),
    url(r'^servicios.html$', ServicesPartialView.as_view(), name='partial-servicios'),
    url(r'^home.html$', T.as_view(template_name="partials/home.html"),
        name='partial-home'),
    url(r'^perfil.html$', T.as_view(template_name="partials/perfil.html"),
        name='partial-perfil'),
    url(r'^solicitudes.html$', T.as_view(template_name="partials/solicitudes.html"),
        name='partial-solicitudes'),
    url(r'^list_applicant.html$', T.as_view(template_name="partials/list_applicant.html"),
        name='partial-list_applicant'),
    url(r'^header_first_step.html$', T.as_view(template_name="partials/header_first_step.html"),
        name='partial-header_first_step'),
    url(r'^first_step.html$', T.as_view(template_name="partials/first_step.html"),
        name='partial-first_step'),
    url(r'^subcategory.html$', T.as_view(template_name="partials/subcategory.html"),
        name='partial-subcategory'),
    url(r'^create_service.html$', T.as_view(template_name="partials/create_service.html"),
        name='partial-create_service'),
)