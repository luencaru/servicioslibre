# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='allow_districts',
            field=models.ManyToManyField(related_name=u'applicants_work', to=u'services.District', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='district',
            field=models.ForeignKey(related_name=u'applicants_from', blank=True, to='services.District', null=True),
        ),
    ]
