# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django_hstore.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('services', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dni', models.CharField(max_length=8, null=True, blank=True)),
                ('address', models.CharField(max_length=100, blank=True)),
                ('mobile_phone', models.CharField(max_length=11, blank=True)),
                ('home_phone', models.CharField(max_length=11, blank=True)),
                ('other_phones', django_hstore.fields.DictionaryField(null=True, blank=True)),
                ('photo', models.ImageField(null=True, upload_to=b'users/photos/', blank=True)),
                ('about_me', models.TextField(blank=True)),
                ('is_applicant', models.BooleanField(default=False)),
                ('is_new', models.BooleanField(default=True)),
                ('url_hash', models.CharField(db_index=True, max_length=40, blank=True)),
                ('verified', models.BooleanField(default=False)),
                ('verified_applicant', models.BooleanField(default=False)),
                ('allow_districts', models.ManyToManyField(related_name=b'applicants_work', to='services.District')),
                ('category', models.ForeignKey(blank=True, to='services.Category', null=True)),
                ('district', models.ForeignKey(related_name=b'applicants_from', to='services.District', null=True)),
                ('subcategories', models.ManyToManyField(to='services.SubCategory', null=True, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TimeAvailability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('monday_since', models.TimeField(null=True, blank=True)),
                ('monday_to', models.TimeField(null=True, blank=True)),
                ('tuesday_since', models.TimeField(null=True, blank=True)),
                ('tuesday_to', models.TimeField(null=True, blank=True)),
                ('wednesday_since', models.TimeField(null=True, blank=True)),
                ('wednesday_to', models.TimeField(null=True, blank=True)),
                ('thursday_since', models.TimeField(null=True, blank=True)),
                ('thursday_to', models.TimeField(null=True, blank=True)),
                ('friday_since', models.TimeField(null=True, blank=True)),
                ('friday_to', models.TimeField(null=True, blank=True)),
                ('saturday_since', models.TimeField(null=True, blank=True)),
                ('saturday_to', models.TimeField(null=True, blank=True)),
                ('sunday_since', models.TimeField(null=True, blank=True)),
                ('sunday_to', models.TimeField(null=True, blank=True)),
                ('owner', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WorkPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'users/jobs/')),
                ('caption', models.TextField(blank=True)),
                ('owner', models.ForeignKey(related_name=b'work_photos', to=settings.AUTH_USER_MODEL)),
                ('subcategory', models.ForeignKey(related_name=b'photos', to='services.SubCategory')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
