# coding=utf-8
from django import forms
from django.contrib.auth import authenticate
from services.models import Category
from .models import Profile, User


class AuthWithEmailForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': "Por favor ingresa un email y contraseña valida. ",
        'inactive': "Esta cuenta esta inactiva",
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthWithEmailForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login'
                )
            elif not self.user_cache.is_active:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive'
                )
        return self.cleaned_data

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class RegisterUserForm(forms.ModelForm):
    first_name = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    category = forms.ModelChoiceField(queryset=Category.objects.all(), empty_label=None, required=False)
    error_messages = {
        'invalid_email': "Este email ya está registrado"
    }

    class Meta:
        model = User
        fields = ('email', 'password', 'first_name')

    def __init__(self, request=None, *args, **kwargs):
        self.user_cache = None
        super(RegisterUserForm, self).__init__(*args, **kwargs)

    def clean(self):
        is_applicant = self.cleaned_data.get('is_applicant', False)
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(
                self.error_messages['invalid_email'],
                code='invalid_email'
            )
        # password = self.cleaned_data.get('password')
        # first_name = self.cleaned_data.get('first_name')
        # category = self.cleaned_data.get('category')
        # u = User.objects.create_user(email, email=email, password=password, first_name=first_name)
        # p = Profile.objects.create(user=u, category=category, is_applicant=is_applicant)
        # # if is_applicant:
        # #     p.create_temporary_link()
        # #     p.save()
        # self.user_cache = authenticate(username=email, password=password)
        self._errors.clear()
        if self.data.get('category'):
            self.cleaned_data['category'] = Category.objects.get(slug=self.data.get('category'))
        return self.cleaned_data

    def save(self, commit=True):
        u = User.objects.create_user(self.cleaned_data.get('email'), email=self.cleaned_data.get('email'),
                                     password=self.cleaned_data.get('password'),
                                     first_name=self.cleaned_data.get('first_name'))
        p = Profile.objects.create(user=u, category=self.cleaned_data.get('category'),
                                   is_applicant=self.data.get('is_applicant', False))

        return u

    def get_user(self):
        return self.user_cache
