from rest_framework.permissions import BasePermission


class IsVerified(BasePermission):
    """
    Allows access only to authenticated verified.
    """

    def has_permission(self, request, view):
        if hasattr(request.user, 'profile') and request.user.profile.verified and (
                    (request.user.profile.is_applicant and request.user.profile.verified_applicant)
                or not request.user.profile.is_applicant):
            return True
        return False