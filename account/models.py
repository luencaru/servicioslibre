import uuid
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django_hstore import hstore
from services.models import Category, SubCategory, Service, District


class TimeAvailability(models.Model):
    monday_since = models.TimeField(null=True, blank=True)
    monday_to = models.TimeField(null=True, blank=True)
    tuesday_since = models.TimeField(null=True, blank=True)
    tuesday_to = models.TimeField(null=True, blank=True)
    wednesday_since = models.TimeField(null=True, blank=True)
    wednesday_to = models.TimeField(null=True, blank=True)
    thursday_since = models.TimeField(null=True, blank=True)
    thursday_to = models.TimeField(null=True, blank=True)
    friday_since = models.TimeField(null=True, blank=True)
    friday_to = models.TimeField(null=True, blank=True)
    saturday_since = models.TimeField(null=True, blank=True)
    saturday_to = models.TimeField(null=True, blank=True)
    sunday_since = models.TimeField(null=True, blank=True)
    sunday_to = models.TimeField(null=True, blank=True)
    owner = models.OneToOneField(User)


class Profile(models.Model):
    dni = models.CharField(null=True, blank=True, max_length=8)
    district = models.ForeignKey(District, related_name='applicants_from', null=True, blank=True)
    address = models.CharField(max_length=100, blank=True)
    mobile_phone = models.CharField(max_length=11, blank=True)
    home_phone = models.CharField(max_length=11, blank=True)
    other_phones = hstore.DictionaryField(null=True, blank=True)
    user = models.OneToOneField(User)
    photo = models.ImageField(upload_to='users/photos/', null=True, blank=True)
    about_me = models.TextField(blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    subcategories = models.ManyToManyField(SubCategory, null=True, blank=True)
    is_applicant = models.BooleanField(default=False)
    is_new = models.BooleanField(default=True)
    allow_districts = models.ManyToManyField(District, related_name='applicants_work', blank=True)
    url_hash = models.CharField(max_length=40, blank=True, db_index=True)
    verified = models.BooleanField(default=True)
    verified_applicant = models.BooleanField(default=True)

    objects = hstore.HStoreManager()

    def create_temporary_link(self):
        self.url_hash = str(uuid.uuid4())

    @property
    def rated_services(self):
        return self.user.done_services.filter(done_score__gt=0).count()

    @property
    def done_services(self):
        return self.user.done_services.filter(done_by=self.user).count()

    def stars_by_subcategory(self, subcategory):
        val = self.user.done_services.filter(subcategory=subcategory).aggregate(Sum('done_score'))['done_score__sum']
        rated_services = self.user.done_services.filter(done_score__gt=0, subcategory=subcategory,
                                                        done_score__isnull=False).count()
        return val / rated_services if val and rated_services else 0

    def last_services(self):
        scs = self.subcategories.values_list('slug', flat=True).all()
        return Service.objects.filter(done_by__isnull=True, subcategory__in=scs).order_by('-created_at')[:5]

    @property
    def photo_url(self):
        return self.photo.url if self.photo else None

    def __unicode__(self):
        return self.user.email


class WorkPhoto(models.Model):
    owner = models.ForeignKey(User, related_name='work_photos')
    image = models.ImageField(upload_to='users/jobs/')
    subcategory = models.ForeignKey(SubCategory, related_name='photos')
    caption = models.TextField(blank=True)

    @property
    def photo_url(self):
        return self.image.url if self.image.url else None

    def __unicode__(self):
        return self.photo_url
