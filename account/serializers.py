from django.contrib.auth.models import User
from drf_compound_fields.fields import ListField, DictField
from rest_framework import serializers
from account.models import WorkPhoto
from .fields import (RatedServicesBySubcategory, WorkPhotoBySubcategory, TotalStarsBySubcategory,
                     DoneServicesBySubcategory)
from services.models import District


class ProfileSerializer(serializers.ModelSerializer):
    about_me = serializers.CharField(source='profile.about_me')
    address = serializers.CharField(source='profile.address')
    photo = serializers.CharField(source='profile.photo_url')
    verified = serializers.CharField(source='profile.verified')
    mobile_phone = serializers.CharField(source='profile.mobile_phone')
    home_phone = serializers.CharField(source='profile.home_phone')
    other_phones = serializers.CharField(source='profile.other_phones')
    work_photos = WorkPhotoBySubcategory(source='*')
    done_services = DoneServicesBySubcategory(source='*')
    total_stars = TotalStarsBySubcategory(source='*')
    rated_services = RatedServicesBySubcategory(source='*')
    reviews_5s = RatedServicesBySubcategory(score=5, source='*')
    reviews_4s = RatedServicesBySubcategory(score=4, source='*')
    reviews_3s = RatedServicesBySubcategory(score=3, source='*')
    reviews_2s = RatedServicesBySubcategory(score=2, source='*')
    reviews_1s = RatedServicesBySubcategory(score=1, source='*')
    category = serializers.CharField(source='profile.category.slug')
    subcategories = serializers.SerializerMethodField('list_subcategories')
    district = serializers.CharField(source='profile.district.name')

    def list_subcategories(self, obj):
        return obj.profile.subcategories.values_list('slug', flat=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'about_me', 'photo', 'work_photos', 'verified', 'reviews_5s',
                  'reviews_4s', 'reviews_3s', 'reviews_2s', 'reviews_1s', 'done_services', 'total_stars',
                  'rated_services', 'category', 'address', 'mobile_phone', 'home_phone', 'other_phones',
                  'subcategories')


class DetailWorkPhotoSerializer(serializers.ModelSerializer):
    image = serializers.CharField(source='photo_url')
    subcategory = serializers.CharField(source='subcategory.name')

    class Meta:
        model = WorkPhoto
        fields = ("image", "subcategory", "caption")


class WorkPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkPhoto
        fields = ("id", "image", "subcategory", "caption")


class FirstStepSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    mobile_phone = serializers.CharField(max_length=11, min_length=9)
    home_phone = serializers.CharField(required=False, max_length=11, min_length=7)
    district = serializers.PrimaryKeyRelatedField(queryset=District.objects.all())
    other_phones = serializers.WritableField(required=False)
    address = serializers.CharField()
    about_me = serializers.CharField(required=False)
    dni = serializers.CharField(min_length=8, max_length=8)
    subcategories = ListField(serializers.SlugField())
    allow_districts = ListField(serializers.CharField())
    availability = ListField(DictField())

    def validate_dni(self, attrs, source):
        value = attrs[source]
        if not value.isdigit():
            raise serializers.ValidationError("dni field must contains digits")
        return attrs

    def validate_availability(self, attrs, source):
        value = attrs[source]
        if not all(d.get('since') and d.get('to') and d.get('day') for d in value):
            raise serializers.ValidationError("'since', 'to' and 'day' must be sent")
        if not all(d['day'] in ('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday') for d in
                   value):
            raise serializers.ValidationError("'day' must be the name of a week day")
        if not all(isinstance(d['since'], int) and isinstance(d['to'], int) for d in value):
            raise serializers.ValidationError("'since' and 'to' must be integers")
        if not all(d['since'] in range(0, 24) and d['to'] in range(0, 24) for d in value):
            raise serializers.ValidationError("'since' and 'to' must be between 0 and 23")
        return attrs


class UserProfileSerializer(serializers.ModelSerializer):
    address = serializers.CharField(source='profile.address')
    image = serializers.SerializerMethodField('get_image')
    mobile_phone = serializers.CharField(source='profile.mobile_phone')
    home_phone = serializers.CharField(source='profile.home_phone')
    dni = serializers.CharField(source='profile.dni')

    def get_image(self, obj):
        return self.context['request'].build_absolute_uri(obj.profile.photo.url) if hasattr(obj.profile.photo,
                                                                                            'url') else None

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'image', 'address', 'mobile_phone', 'home_phone', 'dni')
