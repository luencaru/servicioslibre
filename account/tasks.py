# coding=utf-8
from celery import shared_task
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.mail import EmailMessage


@shared_task
def send_verification_mail(user, *args, **kwargs):
    msg = EmailMessage(subject=u"Activación de tu cuenta", from_email="support@servicioslibre.com", to=[user.email])
    msg.template_name = "VERIFY-ACCOUNT"
    url_activation = settings.HOST + reverse('account:activation', kwargs={'url_hash': user.profile.url_hash})
    msg.global_merge_vars = {'FULLNAME': user.get_full_name(), 'LINK': url_activation}
    msg.send()