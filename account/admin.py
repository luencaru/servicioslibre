from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from .models import Profile, TimeAvailability, WorkPhoto, District


class TimeAvailabilityAdmin(ModelAdmin):
    list_display = ('owner', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')

    def monday(self, obj):
        return "{0.monday_since} - {0.monday_to}".format(obj)

    def tuesday(self, obj):
        return "{0.tuesday_since} - {0.tuesday_to}".format(obj)

    def wednesday(self, obj):
        return "{0.wednesday_since} - {0.wednesday_to}".format(obj)

    def thursday(self, obj):
        return "{0.thursday_since} - {0.thursday_to}".format(obj)

    def friday(self, obj):
        return "{0.friday_since} - {0.friday_to}".format(obj)

    def saturday(self, obj):
        return "{0.saturday_since} - {0.saturday_to}".format(obj)

    def sunday(self, obj):
        return "{0.sunday_since} - {0.sunday_to}".format(obj)

    monday.short_description = 'Monday Availability'


admin.site.register(Profile)
admin.site.register(WorkPhoto)
admin.site.register(TimeAvailability, TimeAvailabilityAdmin)
admin.site.register(District)