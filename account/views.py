# coding=utf-8
from class_based_auth_views.views import LoginView, LogoutView
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http.response import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView, ModelFormMixin
from rest_framework import generics, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from .tasks import send_verification_mail
from .models import WorkPhoto, TimeAvailability, Profile
from .forms import AuthWithEmailForm, RegisterUserForm
from .serializers import ProfileSerializer, WorkPhotoSerializer, DetailWorkPhotoSerializer, FirstStepSerializer, \
    UserProfileSerializer
from services.mixins import NotUserOrApplicantMixin
from services.models import Apply, SubCategory, District, Category
from servicioslibre.generics import MCreateAPiVew
import datetime


class MyProfileAPI(generics.RetrieveAPIView):
    model = User
    serializer_class = ProfileSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        return self.request.user


class ProfileChangePhotoAPI(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        profile = request.user.profile
        if request.FILES:
            photo = request.FILES['photo']
            profile.photo = photo
        profile.save()
        return Response({'photo': profile.photo_url}, status=status.HTTP_200_OK)


class ProfileFirstStepAPI(APIView):
    serializer_class = FirstStepSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=self.request.DATA)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        user = request.user
        profile = user.profile
        for a, b in serializer.data.items():
            if a in ["first_name", "last_name"] and b:
                setattr(user, a, b)
            if a in ["mobile_phone", "home_phone", "address", "about_me", "district"] and b:
                setattr(profile, a, b)
        user.save()
        subcs = list(SubCategory.objects.filter(slug__in=serializer.data.get('subcategories')))
        profile.subcategories.clear()
        profile.subcategories.add(*subcs)
        districts = list(District.objects.filter(postal_code__in=serializer.data.get('allow_districts')))
        profile.allow_districts.clear()
        profile.allow_districts.add(*districts)
        profile.is_new = False
        profile.save()
        ta, created = TimeAvailability.objects.get_or_create(owner=user)
        for a in serializer.data.get('availability'):
            setattr(ta, a['day'] + '_since', datetime.time(a['since']))
            setattr(ta, a['day'] + '_to', datetime.time(a['to']))
        ta.save()
        return Response(status=status.HTTP_200_OK)


class UpdateTimeAvailabilityAPI(APIView):
    model = TimeAvailability
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        availavility, created = self.model.objects.get_or_create(owner=self.request.user)
        data = []
        if not created:
            for day in ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']:
                since = getattr(availavility, day + '_since')
                to = getattr(availavility, day + '_to')
                if since and to:
                    data.append({
                        'day': day,
                        'since': since.strftime("%H:%M:%S"),
                        'to': to.strftime("%H:%M:%S")
                    })
        return Response(data)

    def put(self, request, *args, **kwargs):
        available = request.DATA.get('availability')
        total = {'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'}
        sent = {item['day'] for item in available}
        todrop = total - sent
        ta, created = self.model.objects.get_or_create(owner=self.request.user)
        for a in available:
            setattr(ta, a['day'] + '_since', datetime.time(a['since']))
            setattr(ta, a['day'] + '_to', datetime.time(a['to']))
        for a in todrop:
            setattr(ta, a + '_since', None)
            setattr(ta, a + '_to', None)
        ta.save()
        return Response(status=status.HTTP_200_OK)


class LoginToSiteView(NotUserOrApplicantMixin, LoginView):
    template_name = 'login.html'
    form_class = AuthWithEmailForm


class LogoutToSiteView(LogoutView):
    def get(self, *args, **kwargs):
        raise Http404


class RegisterView(NotUserOrApplicantMixin, CreateView):
    template_name = 'register.html'
    form_class = RegisterUserForm
    success_url = settings.LOGIN_REDIRECT_URL

    def form_invalid(self, form):
        if "second" in self.request.POST:
            self.second = True
        return super(RegisterView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        ctx = super(RegisterView, self).get_context_data(**kwargs)
        ctx['categories'] = Category.objects.all()
        if hasattr(self, 'second'):
            ctx['second'] = True
        return ctx

    # def form_valid(self, form):
    #     self.object = form.save()
    #     login(self.request, self.object)
    #     messages.add_message(self.request, messages.SUCCESS,
    #                          '¡Bienvenido! te hemos enviado un mensaje a tu email para que valides tu cuenta.')
    #     # send_verification_mail.delay(form.get_user())
    #     return super(RegisterView, self).form_valid(form)
    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, self.object)
        return super(ModelFormMixin, self).form_valid(form)


class ActivateAccountView(DetailView):
    model = Profile
    template_name = "activate.html"
    slug_field = 'url_hash'

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        if not obj.verified:
            obj.verified = True
            obj.save()
            return super(ActivateAccountView, self).get(request, *args, **kwargs)
        else:
            raise Http404


class RegisterAPIView(generics.GenericAPIView):
    def post(self, *args, **kwargs):
        email = self.request.DATA.get('email')
        password = self.request.DATA.get('password')
        u, c = User.objects.get_or_create(username=email, defaults={"email": email})
        if c:
            u.set_password(password)
            u.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class DetailUserAPI(generics.RetrieveAPIView):
    model = User
    serializer_class = ProfileSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        ap = self.request.GET.get('apply')
        if ap:
            ap = get_object_or_404(Apply, id=self.kwargs.get('pk'))
            self.subcategory = ap.service.subcategory
            return ap.owner
        return self.request.user


class UploadWorkPhotoAPI(MCreateAPiVew):
    model = WorkPhoto
    serializer_class = WorkPhotoSerializer
    response_serializer_class = DetailWorkPhotoSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def pre_save(self, obj):
        obj.owner = self.request.user

    def get_queryset(self):
        return self.request.user.work_photos.all()


class DeleteWorkPhoto(generics.DestroyAPIView):
    model = WorkPhoto
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)


class ListUserProfileAPI(ListAPIView):
    authentication_classes = ()
    permission_classes = (AllowAny,)
    queryset = User.objects.filter(is_superuser=False).prefetch_related('profile')
    serializer_class = UserProfileSerializer
    paginate_by = 10

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(self.object_list)
        if page is not None:
            serializer = self.get_pagination_serializer(page)
        else:
            serializer = self.get_serializer(self.object_list, many=True)

        return Response(serializer.data)
