from django.db.models import Sum
from rest_framework import fields


class WorkPhotoBySubcategory(fields.Field):
    def to_native(self, user):
        request = self.context.get('request', None)
        view = self.context.get('view', None)
        if request and not request.user.is_superuser and hasattr(view, 'subcategory'):
            subcategory = view.subcategory
            return [{'image': p.photo_url, 'subcategory': p.subcategory.name, 'caption': p.caption} for p in
                    user.work_photos.filter(subcategory=subcategory)]
        return [{'image': p.photo_url, 'subcategory': p.subcategory.name, 'caption': p.caption} for p in
                user.work_photos.all()]


class DoneServicesBySubcategory(fields.Field):
    def to_native(self, user):
        request = self.context.get('request', None)
        view = self.context.get('view', None)
        if request and not request.user.is_superuser and hasattr(view, 'subcategory'):
            subcategory = view.subcategory
            return user.done_services.filter(subcategory=subcategory).count()
        return user.done_services.count()


class TotalStarsBySubcategory(fields.Field):
    def to_native(self, user):
        request = self.context.get('request', None)
        view = self.context.get('view', None)
        if request and not request.user.is_superuser and hasattr(view, 'subcategory'):
            subcategory = view.subcategory
            val = user.done_services.filter(subcategory=subcategory).aggregate(Sum('done_score'))
            return val['done_score__sum'] if val['done_score__sum'] else 0
        val = user.done_services.aggregate(Sum('done_score'))
        return val['done_score__sum'] if val['done_score__sum'] else 0


class RatedServicesBySubcategory(fields.Field):
    def __init__(self, score=None, *args, **kwargs):
        self.score = score
        super(RatedServicesBySubcategory, self).__init__(*args, **kwargs)

    def to_native(self, user):
        request = self.context.get('request', None)
        view = self.context.get('view', None)
        if request and not request.user.is_superuser and hasattr(view, 'subcategory'):
            subcategory = view.subcategory
            if self.score:
                return user.done_services.filter(done_score=self.score, subcategory=subcategory).count()
            return user.done_services.filter(done_score__gt=0, subcategory=subcategory).count()
        if self.score:
            return user.done_services.filter(done_score=self.score).count()
        return user.done_services.filter(done_score__gt=0).count()