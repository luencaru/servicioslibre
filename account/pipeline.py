from urllib import urlopen
from django.core.files.base import ContentFile
from django.template.defaultfilters import slugify
from social.backends.facebook import FacebookOAuth2
from .models import Profile


def get_user_avatar(backend, response, user, *args, **kwargs):
    if kwargs.get('is_new'):
        if backend.__class__ == FacebookOAuth2:
            profile = Profile.objects.get_or_create(user=user)[0]
            url = "http://graph.facebook.com/%s/picture?type=large" % response["id"]
            avatar = urlopen(url)
            profile.photo.save(slugify(user.email + " social") + '.jpg',
                               ContentFile(avatar.read()))
            profile.verified = True
            profile.save()