from django.conf.urls import patterns, url
from account.views import DeleteWorkPhoto
from inbox.views import ListPossibleLinksAPI
from .views import (MyProfileAPI, LoginToSiteView, RegisterView, UploadWorkPhotoAPI, ProfileFirstStepAPI,
                    UpdateTimeAvailabilityAPI, ProfileChangePhotoAPI, RegisterAPIView, ActivateAccountView,
                    LogoutToSiteView, ListUserProfileAPI)

urlpatterns = patterns(
    '',
    url(r'^api/users/$', ListUserProfileAPI.as_view(), name="users"),
    url(r'^api/me/$', MyProfileAPI.as_view(), name="me"),
    url(r'^api/me/possible-links/', ListPossibleLinksAPI.as_view(), name="me"),
    url(r'^api/me/first-step/$', ProfileFirstStepAPI.as_view(), name="first-step"),
    url(r'^api/me/profile-photo/$', ProfileChangePhotoAPI.as_view(), name="change-photo"),
    url(r'^api/me/work-photos/$', UploadWorkPhotoAPI.as_view(), name="work-photos"),
    url(r'^api/me/work-photos/(?P<pk>\d+)/$', DeleteWorkPhoto.as_view(), name="delete-work-photos"),
    url(r'^api/me/availability/$', UpdateTimeAvailabilityAPI.as_view(), name="update-availability"),
    url(r'^login/$', LoginToSiteView.as_view(), name="login"),
    url(r'^logout/$', LogoutToSiteView.as_view(), name="logout"),
    url(r'^api/register/$', RegisterAPIView.as_view(), name="api-register"),
    url(r'^registro/$', RegisterView.as_view(), name="register"),
    url(r'^activacion/(?P<slug>\S+)/$', ActivateAccountView.as_view(), name="activation"),
)