from django.contrib.auth.models import User
from django.db import models


class Notification(models.Model):
    RECOMMENDED = 'RECOMMENDED'
    CONNECTED = 'CONNECTED'
    COUNTER_PROPOSAL = 'COUNTER_PROPOSAL'
    ACCEPTED_PROPOSAL = 'ACCEPTED_PROPOSAL'
    NEW_MESSAGE = 'NEW_MESSAGE'
    NEW_APPLY = 'NEW_APPLY'
    created_at = models.DateTimeField(auto_now_add=True)
    seen_at = models.DateTimeField(null=True, blank=True)
    type = models.CharField(max_length=20, choices=(
        (RECOMMENDED, 'Has sido recomendado para un servicio'),
        (CONNECTED, 'Han conectado una de tus propuestas'),
        (COUNTER_PROPOSAL, 'El cliente te ha hecho una contrapropuesta'),
        (ACCEPTED_PROPOSAL, 'El cliente ha aceptado tu propuesta'),
        (NEW_MESSAGE, 'Te han dejado un mensaje en tu propuesta'),
        (NEW_APPLY, 'Alguien ha aplicado a tu servicio')
    ))
    owner = models.ForeignKey(User)
    object_id = models.PositiveIntegerField()

    class Meta:
        ordering = ('-created_at',)
