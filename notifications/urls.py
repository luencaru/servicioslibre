from django.conf.urls import patterns, url
from .views import ListNotificationsAPI, MarkAsReadNotificationAPI

urlpatterns = patterns(
    '',
    url(r'api/me/notifications/', ListNotificationsAPI.as_view(), name='my-notifications'),
    url(r'^api/notifications/(?P<pk>\d+)/read/$', MarkAsReadNotificationAPI.as_view(), name="mark-as-read-notification")
)

