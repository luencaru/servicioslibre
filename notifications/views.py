from django.utils.timezone import now
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from .models import Notification
from account.permissions import IsVerified


class ListNotificationsAPI(generics.ListAPIView):
    model = Notification
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    paginate_by = 5

    def get_queryset(self):
        return self.model.objects.filter(owner=self.request.user)


class MarkAsReadNotificationAPI(APIView):
    model = Notification
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsVerified)

    def post(self, request, *args, **kwargs):
        notification_id = self.kwargs.get('pk')
        notification = self.model.objects.filter(id=notification_id).first()
        if notification and self.request.user == notification.owner:
            notification.seen_at = now()
            notification.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)