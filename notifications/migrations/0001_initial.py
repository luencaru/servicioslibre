# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('seen_at', models.DateTimeField(null=True, blank=True)),
                ('type', models.CharField(max_length=20, choices=[(b'RECOMMENDED', b'Has sido recomendado para un servicio'), (b'CONNECTED', b'Han conectado una de tus propuestas'), (b'COUNTER_PROPOSAL', b'El cliente te ha hecho una contrapropuesta'), (b'ACCEPTED_PROPOSAL', b'El cliente ha aceptado tu propuesta'), (b'NEW_MESSAGE', b'Te han dejado un mensaje en tu propuesta'), (b'NEW_APPLY', b'Alguien ha aplicado a tu servicio')])),
                ('object_id', models.PositiveIntegerField()),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_at',),
            },
            bases=(models.Model,),
        ),
    ]
